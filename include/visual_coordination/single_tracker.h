#ifndef ROS_SINGLE_TRACKER_H_
#define ROS_SINGLE_TRACKER_H_

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include "visual_coordination/MFeatures.h"

#include "vservoing.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <sstream>
#include <iostream>
#include <string>


class SingleTracker{

 protected:
  
  ros::NodeHandle node;

  image_transport::ImageTransport itr;
  
  std::string topicName;

  std::string windowName;

  CVEcran myDisplay;
  
  std::string cameraName;

  std::string targetName;

  std::string frameId;

  cv::Mat lastImage;
 
  image_transport::Subscriber image_sub;

  ros::Publisher feat_pub;

  ros::Subscriber feat_sub;

  Eigen::VectorXd S;

 public:

  SingleTracker(std::string topic, std::string window, std::string camera, std::string target);

  ~SingleTracker();

  virtual void realTrack();

  virtual void resetTracker(const Eigen::VectorXd & SS);

  void imageCallback(const sensor_msgs::ImageConstPtr & msg);

  void resetCallback(const visual_coordination::MFeaturesConstPtr & msg);

  void publishVisualCues(const Eigen::VectorXd & SS);
  
  		     

}; 


#endif
