#ifndef ROS_ROBOT_SYSTEM_H_
#define ROS_ROBOT_SYSTEM_H_

#include <ros/ros.h>
#include <control_msgs/JointTrajectoryAction.h>
#include <sensor_msgs/JointState.h>
#include <actionlib/client/simple_action_client.h>
#include "vservoing.h"

typedef actionlib::SimpleActionClient< control_msgs::JointTrajectoryAction > TrajClient;

class PR2Arms: public TaskSystem{

 protected:

  int armDOF;

  int totalDOF;

  int K;

  double period;

  double deadline;

  bool waitFor;

  bool controllable;

  ros::NodeHandle node;
  
  Eigen::VectorXd state;

  Eigen::VectorXd velocity;

  Eigen::VectorXd control;

  Eigen::VectorXd prediction;

  std::vector<std::string> joint_names;

  TrajClient * left_arm_client;

  TrajClient * right_arm_client;

  ros::Subscriber sub_joint;

  virtual void pushTrajectory();

  void retrieveCurrentState();

  void receiveCurrentState(const sensor_msgs::JointStateConstPtr & msg);

 public:

  PR2Arms();

  ~PR2Arms();

  int getDimState() const;

  Eigen::VectorXd getState() const;

  void describeState() const;

  void setState(const Eigen::VectorXd & state);

  void takeCommand(const Eigen::VectorXd & order);

  void enableControl(bool choice);

  void updateTime(double t);

  double getPeriod() const;

};

#endif
