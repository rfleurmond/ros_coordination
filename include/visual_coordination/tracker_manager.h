#ifndef ROS_TRACKER_MANAGER_H_
#define ROS_TRACKER_MANAGER_H_

#include "single_tracker.h"
#include <map>
#include <vector>

class AssocTracker{

 public:

  AssocTracker();

  AssocTracker(Camera * ,std::string, std::string);

  Camera * eye;

  std::string cameraName;

  std::string targetName;

  int length;

  Eigen::VectorXd S;

  Eigen::VectorXi seen;

  bool lost;

  int updates;

  ros::Subscriber sub_track;

  ros::Publisher pub_track;


};

class TrackerManager{

  friend class SmallTracker;

 protected:
  
  ros::NodeHandle node;

  std::map<std::string, AssocTracker> fonctions;

  std::map<Camera *, std::string > cameras;

  std::vector<VTracker * > indicateurs;

  std::map<std::string, std::string > frames;

 public:

  TrackerManager();

  ~TrackerManager();

  void trackerCallback(const visual_coordination::MFeaturesConstPtr & msg);

  void addCamera(Camera *, std::string);

  VTracker *  createTracker(std::string, int dim);

  VTracker *  createTracker(std::string, VTracker * );

  void recordCouple(Camera * ,std::string, std::string, int dim = 3);
  
}; 


class SmallTracker: public VTracker{

 private:
 
  std::string targetName;

  TrackerManager * master;

  int dim;

  VTracker * libTracker;

 public: 

  SmallTracker(TrackerManager *, std::string, int dim);

  SmallTracker(TrackerManager *, std::string, VTracker * );

  virtual int getDimensionOutput() const;

  virtual bool itCanBeSeen() const;

  virtual bool isLost() const;

  virtual Eigen::VectorXd getFunction() const;  

  virtual void findFeatures(const Eigen::VectorXd & SS);  

  virtual void draw(CVEcran * const vue, cv::Scalar couleur) const;

  virtual void drawFeat(const Eigen::VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const;

};

#endif
