#include <ros/ros.h>
#include <control_msgs/GripperCommandAction.h>
#include <actionlib/client/simple_action_client.h>

// Our Action interface type, provided as a typedef for convenience
typedef actionlib::SimpleActionClient<control_msgs::GripperCommandAction> GripperClient;

class Gripper{

private:

  GripperClient* gripper_client_;  

public:
  //Action client initialization
  Gripper(int side);

  ~Gripper();

  static const int RIGHT_GRIPPER = 0;
  static const int LEFT_GRIPPER = 1;

  //Open the gripper
  void open();

  //Close the gripper
  void close();

};
