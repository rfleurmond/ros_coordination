#ifndef ROS_PR2_HEAD_ARMS_H_
#define ROS_PR2_HEAD_ARMS_H_

#include "ros_robot_system.h"


class PR2ArmsHead: public PR2Arms{

 protected:

  int headDOF;

  TrajClient * head_client;

  void pushTrajectory();

 public:

  PR2ArmsHead();

  ~PR2ArmsHead();
  
};

#endif
