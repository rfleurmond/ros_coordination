#ifndef MY_ROS_CAMERA_H_
#define MY_ROS_CAMERA_H_

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include "vservoing.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <sstream>
#include <iostream>
#include <string>
#include <vector>



class MyRosCamera: public Camera{
  
 private: 

  std::string name;
  
  std::string topicName;

  CVEcran myDisplay;
  
  std::string frameId;

  image_transport::ImageTransport itr;

  image_transport::Subscriber image_sub;

  cv::Mat lastImage;

  std::list<RealTracker *> followers;

 public: 

  MyRosCamera(ros::NodeHandle node, std::string name, std::string topic, const IntrinsicCam & param);

  ~MyRosCamera();

  cv::Mat getLastImage() const;

  void imageCallback(const sensor_msgs::ImageConstPtr & msg);

  void addFollowers(RealTracker * tracker);

  CVEcran * getDisplay();

};

#endif
