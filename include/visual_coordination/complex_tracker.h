#ifndef ROS_COMPLEX_TRACKER_H_
#define ROS_COMPLEX_TRACKER_H_

#include "vservoing.h"

class ComplexTracker: public VTracker{

 private:
 
  std::string targetName;

  VTracker * master;

  std::map<Camera * , VTracker * > employees;

 public: 

  ComplexTracker(std::string, VTracker * );

  void addInternalTracker(Camera *, VTracker *); 

  virtual int getDimensionOutput() const;

  virtual bool itCanBeSeen() const;

  virtual bool isLost() const;

  virtual Eigen::VectorXd getFunction() const;  

  virtual void findFeatures(const Eigen::VectorXd & SS);  

  virtual void draw(CVEcran * const vue, cv::Scalar couleur) const;

  virtual void drawFeat(const Eigen::VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const;

};

  


#endif
