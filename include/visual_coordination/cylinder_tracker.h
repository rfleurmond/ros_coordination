#ifndef ROS_CYL_TRACKER_H_
#define ROS_CYL_TRACKER_H_

#include "single_tracker.h"

class CylTracker: public SingleTracker{

 private:

  CylinderTracker glasses;

  Camera eye;

 public:

  CylTracker(std::string topic, std::string window, std::string camera, std::string target);

  void realTrack();

  void resetTracker(const Eigen::VectorXd & SS);

};

#endif
