#ifndef ROS_TASK_CONTROLLER_H_
#define ROS_TASK_CONTROLLER_H_

#include "taches.h"
#include <ros/ros.h>

/**
   @brief A Ros adaptation of the TaskController

   @author Renliw Fleurmond
 */


class ROSTaskController: public TaskController{

 public:

  ROSTaskController(TaskSystem * sys, Task * task);
  
  void run();

};

#endif
