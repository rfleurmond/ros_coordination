#ifndef ROS_LKCOLOR_TRACKER_H_
#define ROS_LKCOLOR_TRACKER_H_

#include "single_tracker.h"

class LKColorTracker: public SingleTracker{

 private:

  SpecialTracker glasses;

  Camera eye;

 public:

  LKColorTracker(std::string topic, std::string window, std::string camera, std::string target);

  void realTrack();

  void resetTracker(const Eigen::VectorXd & SS);

};

#endif
