#include "gripper.h"

int main(int argc, char** argv){

  ros::init(argc, argv, "closeRight");

  Gripper gripper(Gripper::RIGHT_GRIPPER);

  gripper.close();

  return 0;
}
