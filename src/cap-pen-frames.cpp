#include <ros/ros.h>
#include <tf/transform_broadcaster.h>

int main(int argc, char** argv){
  ros::init(argc, argv, "my_tf_broadcaster");
  ros::NodeHandle node;

  tf::TransformBroadcaster br;
  tf::Transform transformCap;
  tf::Transform transformPen;

  
  ros::Rate rate(50.0);
  while (node.ok()){
    transformCap.setOrigin( tf::Vector3(0.17, 0.0, 0.0 - 0.1375));
    transformCap.setRotation( tf::Quaternion(0, 0, 0));
    
    transformPen.setOrigin( tf::Vector3(0.17, 0.0, 0.0));
    transformPen.setRotation( tf::Quaternion(0, 0, 0));
    
    br.sendTransform(
		     tf::StampedTransform(transformPen,
					  ros::Time::now(),
					  "r_wrist_roll_link", "my_pen"));
    br.sendTransform(
		     tf::StampedTransform(transformCap, 
					  ros::Time::now(),
					  "l_wrist_roll_link", "my_cap"));
    rate.sleep();
  }
  return 0;
};
