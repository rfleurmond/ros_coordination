#include <ros/ros.h>
#include <control_msgs/JointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include "outils.h"

typedef actionlib::SimpleActionClient< control_msgs::JointTrajectoryAction > TrajClient;

class RobotArm
{
private:
  // Action client for the joint trajectory action 
  // used to trigger the arm movement action
  TrajClient* traj_clientL_;
  TrajClient* traj_clientR_;
  
  int temps;
  int cmdGauche;
  int cmdDroite;
  int qGauche;
  int qDroite;
  int periode;
  double pause;
  std::vector<double *> data;
  

public:
  //! Initialize the action client and wait for action server to come up
  RobotArm() 
  {

    temps = 0;
    cmdGauche = 1;//1 pour le temps 
    cmdDroite = 8;// 1+7 degres d'un bras = 7
    qGauche  = 15; //8+7
    qDroite  =  22;//15+7
    periode = 1;
    pause = 2.0 ; // 2 secondes

    data = loadDAT("/home/rfleurmo/Documents/THESE_LAAS/CODES/C++/build/data_sys.dat",29);//22+7

    std::cout<<"Nombre d'iterations "<<data.size()<<std::endl;
    
    // tell the action client that we want to spin a thread by default
    traj_clientL_ = new TrajClient("l_arm_controller/joint_trajectory_action", true);
    traj_clientR_ = new TrajClient("r_arm_controller/joint_trajectory_action", true);

    // wait for action server to come up
    while(!traj_clientL_->waitForServer(ros::Duration(5.0))){
      ROS_INFO("Waiting for the joint_trajectory_action server");
    }
    while(!traj_clientR_->waitForServer(ros::Duration(5.0))){
      ROS_INFO("Waiting for the joint_trajectory_action server");
    }

    std::cout<<"Connection aux deux serveurs!? "<<std::endl;
    
  }

  //! Clean up the action client
  ~RobotArm()
  {
    delete traj_clientL_;
    delete traj_clientR_;
    freeDATA(data);
  }

  //! Sends the command to start a given trajectory
  void startTrajectory(control_msgs::JointTrajectoryGoal goalL, control_msgs::JointTrajectoryGoal goalR)
  {
    // When to start the trajectory: 1s from now
    goalL.trajectory.header.stamp = ros::Time::now() + ros::Duration(1.0);
    goalR.trajectory.header.stamp = goalL.trajectory.header.stamp;
    traj_clientL_->sendGoal(goalL);
    traj_clientR_->sendGoal(goalR);
  }

  //! Generates a simple trajectory with two waypoints, used as an example
  /*! Note that this trajectory contains two waypoints, joined together
      as a single trajectory. Alternatively, each of these waypoints could
      be in its own trajectory - a trajectory can have one or more waypoints
      depending on the desired application.
  */

  void armExtensionTrajectoryRL(control_msgs::JointTrajectoryGoal & goal, int sens)
  {
    //sens = 1 pour gauche, autre valeur pour droite
    
    // We will have two waypoints in this goal trajectory
    goal.trajectory.points.resize(data.size());
    
    // First trajectory point
    goal.trajectory.points[0].time_from_start = ros::Duration(pause);

    // Positions
    int ind = 0;
    double * instant = NULL;

    int position = qDroite;
    int vitesses = cmdDroite;
    
    if(sens==1){
      position = qGauche;
      vitesses = cmdGauche;
    }
    std::vector<double * >::iterator iterateur;

    for(iterateur = data.begin();iterateur!=data.end();iterateur++){
      
      double * instant = *iterateur;

      // Positions
      goal.trajectory.points[ind].positions.resize(7);
      for (size_t j = 0; j < 7; ++j){
	goal.trajectory.points[ind].positions[j] = instant[position+j];
      }
    
      goal.trajectory.points[ind].time_from_start = ros::Duration(pause + instant[temps]);

      /*
      // Velocities
      goal.trajectory.points[ind].velocities.resize(7);
      for (size_t j = 0; j < 7; ++j){
	goal.trajectory.points[ind].velocities[j] = instant[vitesses+j];
      }
      */
      ind+=periode;
      
    }

    goal.trajectory.points[data.size()-1].velocities.resize(7);
    goal.trajectory.points[0].velocities.resize(7);
    for (size_t j = 0; j < 7; ++j){
      goal.trajectory.points[data.size()-1].velocities[j] = 0.0;
      goal.trajectory.points[0].velocities[j] = 0.0;
    }
    
  }



control_msgs::JointTrajectoryGoal armExtensionTrajectoryR()
  {
    //our goal variable
    control_msgs::JointTrajectoryGoal goal;

    // First, the joint names, which apply to all waypoints
    goal.trajectory.joint_names.push_back("r_shoulder_pan_joint");
    goal.trajectory.joint_names.push_back("r_shoulder_lift_joint");
    goal.trajectory.joint_names.push_back("r_upper_arm_roll_joint");
    goal.trajectory.joint_names.push_back("r_elbow_flex_joint");
    goal.trajectory.joint_names.push_back("r_forearm_roll_joint");
    goal.trajectory.joint_names.push_back("r_wrist_flex_joint");
    goal.trajectory.joint_names.push_back("r_wrist_roll_joint");
    
    armExtensionTrajectoryRL(goal, 0);
      
    //we are done; return the goal
    return goal;
  }

control_msgs::JointTrajectoryGoal armExtensionTrajectoryL()
  {
    //our goal variable
    control_msgs::JointTrajectoryGoal goal;

    // First, the joint names, which apply to all waypoints
    goal.trajectory.joint_names.push_back("l_shoulder_pan_joint");
    goal.trajectory.joint_names.push_back("l_shoulder_lift_joint");
    goal.trajectory.joint_names.push_back("l_upper_arm_roll_joint");
    goal.trajectory.joint_names.push_back("l_elbow_flex_joint");
    goal.trajectory.joint_names.push_back("l_forearm_roll_joint");
    goal.trajectory.joint_names.push_back("l_wrist_flex_joint");
    goal.trajectory.joint_names.push_back("l_wrist_roll_joint");
    
    armExtensionTrajectoryRL(goal, 1);
      
    //we are done; return the goal
    return goal;
  }

  //! Returns the current state of the action
  actionlib::SimpleClientGoalState getStateL()
  {
    return traj_clientL_->getState();
  }

  actionlib::SimpleClientGoalState getStateR()
  {
    return traj_clientR_->getState();
  }
 
};

int main(int argc, char** argv)
{
  // Init the ROS node
  std::cout<<"Initialisation du noeur ROS"<<std::endl;
  ros::init(argc, argv, "robot_driver");
  std::cout<<"Lancement du contructeur"<<std::endl;
  RobotArm arms;
  // Start the trajectory
  std::cout<<"Demarrer la trajectoire"<<std::endl;
  arms.startTrajectory(arms.armExtensionTrajectoryL(),arms.armExtensionTrajectoryR());
  // Wait for trajectory completion
  std::cout<<"Juste avant la boucle"<<std::endl;
  while(!arms.getStateL().isDone() && !arms.getStateR().isDone() && ros::ok())
  {
    usleep(50000);
  }
}
