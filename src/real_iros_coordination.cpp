#include "ros_task_controller.h"
#include "ros_robot_system.h"
#include "tracker_manager.h"
#include "my_ros_camera.h"
#include "complex_tracker.h"
#include "vservoing.h"

using namespace Eigen;
using namespace std;

int main(int argc, char** argv)
{
  // Init the ROS node
  ros::init(argc, argv, "renliw_coord");

  ros::NodeHandle node;
  
  VectorXd but(14);

  but << 
    M_PI/4,  -0.1,  M_PI/2,  -M_PI/2-0.2, -M_PI/2,  -0.05,  0,
    -M_PI/4,  0.1, -M_PI/2,  -M_PI/2-0.2,  M_PI/2,  -0.05,  0;
  
  //but = VectorXd::Zero(14);

  double L = 0.075;

  double d2r = M_PI/180;

  double r2d = 360/M_PI;

  IntrinsicCam headRight(417.66329, 417.88768, 318.43842, 221.30017);
  IntrinsicCam headLeft( 421.96246, 422.28643, 317.76621, 224.33886);
  IntrinsicCam rightArm( 430.67783, 430.19755, 324.72594, 236.54259);
  IntrinsicCam leftArm(  424.97353, 427.11019, 324.51727, 228.79747);
    
  MyRosCamera rightEye(node,"RightEye","/r_forearm_cam/image_raw", rightArm);
  MyRosCamera leftEye(node,"LeftEye","/wide_stereo/left/image_raw",headLeft);
  
  
  Vector3d p0(0, 0, 0);

  Vector3d p1(0, 0, L);
  
  LineWithK cap(p0,p1,p0);

  LineWithK pen(p0,p1,p1);

  SpecialTracker trackRightCap;
  SpecialTracker trackRightPen;
  SpecialTracker trackLeftCap;
  SpecialTracker trackLeftPen;

  ComplexTracker capTracker("cap",&trackRightCap);
  capTracker.addInternalTracker(&rightEye,&trackRightCap);
  capTracker.addInternalTracker(&leftEye,&trackLeftCap);
  
  ComplexTracker penTracker("pen",&trackRightPen);
  penTracker.addInternalTracker(&rightEye,&trackRightPen);
  penTracker.addInternalTracker(&leftEye,&trackLeftPen);
  

  TrackerManager client_serveur;

  client_serveur.addCamera(&leftEye,"LeftCam");

  client_serveur.addCamera(&rightEye,"RightCam");

  
  /*
  LineKEstimator capEstimator(&cap,&capTracker);

  LineKEstimator penEstimator(&pen,&penTracker);

  // */

  //*

  LineKEstimator capEstimator(&cap,&cap);

  LineKEstimator penEstimator(&pen,&pen);

  // */

  //capEstimator.setLogFile("estimation-cap.dat");

  capEstimator.setThreshold(200);

  //penEstimator.setLogFile("estimation-pen.dat");

  penEstimator.setThreshold(200);



  //Gestion des bras du robot

  //Creation des modeles vides et des bras

  GeoRobot brasGauche,brasDroite;

  //Chargement du modele geometrique des bras

  loadSE3Robot("/home/rfleurmo/Documents/THESE_LAAS/CODES/C++/robot_models/pr2_gauche.ser",brasGauche);

  loadSE3Robot("/home/rfleurmo/Documents/THESE_LAAS/CODES/C++/robot_models/pr2_droite.ser",brasDroite);

  //Creation des modeles cinematiques des bras

  MyKineModel modeleGauche(&brasGauche);

  MyKineModel modeleDroite(&brasDroite);

  

  ManyArmHolderTask tache(true);

  // Defintion des reperes

  Repere epauleGauche(VectRotation(0,0,0),Vector3d(0,  0.188, 0));
  
  Repere epauleDroite(VectRotation(0,0,0),Vector3d(0, -0.188, 0));

  Repere priseGauche(VectRotation(0, 0, 0),Vector3d(0.17, 0,-L));
  
  Repere priseDroite(VectRotation(0, 0, 0),Vector3d(0.17, 0, 0.00));

  Repere camGauche(VectRotation(0,M_PI/4,0),Vector3d(0.067, 0.029, 0.4968));

  Repere camDroite(VectRotation(0,M_PI/4,0),Vector3d(0.067,-0.060, 0.4968));

  Repere fixDroite(VectRotation(3.018, -0.872524,0),
		   Vector3d(0.135-0.321, 0.044, 0));

  Repere simple;

  // Ajout des deux bras a la tache

  tache.addArm(&modeleGauche,epauleGauche);

  tache.addArm(&modeleDroite,epauleDroite);

  // Defintion des parametres systemes de la tache

  tache.setPeriod(1.0/15);

  double lambda = 1.5;

  tache.setLambda(lambda);

  tache.setMinimumErrorNorm(1e-2);

  //Placement des caméras

  tache.addCamera(&rightEye,&modeleDroite,fixDroite,5,"Moving Right Camera");

  tache.addCamera(&leftEye,NULL,camGauche,7,"Fixed Head Left camera");

  // Placement des indices visuels

  tache.addVisualTarget(&capEstimator,&modeleGauche,priseGauche,7);

  tache.addVisualTarget(&penEstimator,&modeleDroite,priseDroite,7);

  // Quels indices visuels percoivent les cameras?

  tache.makeTargetSeen(&capEstimator,&leftEye);

  tache.makeTargetSeen(&capEstimator,&rightEye);

  tache.makeTargetSeen(&penEstimator,&leftEye);

  tache.makeTargetSeen(&penEstimator,&rightEye);


  // Quelle est la reference des indices visuels par camera?
  
  penEstimator.attachToCamera(&leftEye);

  OtherReference ref1(&penEstimator);

  tache.giveTargetReference(&leftEye,&capEstimator,&ref1,true);

  penEstimator.attachToCamera(&rightEye);

  OtherReference ref2(&penEstimator);

  tache.giveTargetReference(&rightEye,&capEstimator,&ref2,true);

  // Constante arbitraire pour l'écart entre le stylo et le capuchon
  
  VectorXd constante(3);
  
  constante << 0, 0, 50;

  tache.giveKandCmatrix(&rightEye,&capEstimator,MatrixXd::Identity(3,3),constante);
  
  tache.setState(but);
  
  //Defintion de la premiere tâche a enchainer

  // alignement des deux axes

  MatrixXd selection1 = MatrixXd::Identity(3,6);

  selection1(1,1) = r2d;

  PartTask coplanaire(selection1,&tache);

  coplanaire.setMinimumErrorNorm(2);

  //coplanaire.setLambda(AdaptiveGain(lambda,5*lambda));

  
  // Definition de la deuxieme tache a enchainer

  MatrixXd selection2 = MatrixXd::Identity(5,6);

  selection2(1,1) = r2d;

  selection2(4,4) = r2d;

  PartTask alignement(selection2,&tache);

  alignement.setMinimumErrorNorm(2);

  //alignement.setLambda(AdaptiveGain(lambda,5*lambda));

  //Defintion de la troisieme tache a enchainer
  
  MatrixXd selection3 = MatrixXd::Zero(5,6);

  selection3.block<5,6>(0,0) <<
    1,   0, 0, 0,   0, 0,
    0, r2d, 0, 0,   0, 0,
    0,   0, 0, 1,   0, 0,
    0,   0, 0, 0, r2d, 0,
    0,   0, 0, 0,   0, 1;
  
  PartTask translation(selection3,&tache);

  translation.setMinimumErrorNorm(2);

  //translation.setLambda(AdaptiveGain(lambda,5*lambda));


  //Definition de la sequence de tâches
  
  SequencyTask sequence(&coplanaire,&alignement);

  sequence.addTaskInStack(&translation);

  //Gestion des butees

  VectorXd buteeMin(14);

  VectorXd buteeMax(14);

  buteeMin.topRows(7) = brasGauche.getMinimalQ();
  buteeMin.bottomRows(7) = brasDroite.getMinimalQ();

  buteeMax.topRows(7) = brasGauche.getMaximalQ();
  buteeMax.bottomRows(7) = brasDroite.getMaximalQ();

  MatrixXd selection5 = MatrixXd::Zero(10,14);

  selection5.block<4,4>(0,0) = MatrixXd::Identity(4,4);
  selection5.block<4,4>(5,7) = MatrixXd::Identity(4,4);
  selection5(4,5)  = 1;
  selection5(9,12) = 1;

  buteeMin = selection5 * buteeMin;
  buteeMax = selection5 * buteeMax;

  QBoundedTask gendarme(buteeMin,buteeMax,selection5);

  gendarme.setCoeffMax(-0.01);

  gendarme.setCoeffMin(0.05);

  gendarme.setAlpha(2);

  gendarme.setMax(10);
  
  gendarme.setState(but);

  gendarme.setLambda(2);


  /// CONNECTION AU ROBOT PR2
  
  PR2Arms C3P0;

  C3P0.setState(but);

  //rightEye.addFollowers(&trackRightCap);
  //rightEye.addFollowers(&trackRightPen);
  //leftEye.addFollowers(&trackLeftCap);
  //leftEye.addFollowers(&trackLeftPen);
  
  tache.setState(C3P0.getState());
  

  /// VERIFICATION DE CERTAINES VALEURS

  VectorXd val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  MatrixXd J = tache.jacobien(but);

  cout <<"Jacobien de la tache\n"<< J <<endl;

  FullPivLU<MatrixXd> lu_decomp(J);

  cout << "The rank of J is " << lu_decomp.rank() << endl;

  cout <<"Etat du systeme\n"<<but<<endl;

  val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  MyOptimaPriorityTask globale(&gendarme,&sequence);

  globale.setState(but);

  //globale.hideTaskOnControl(&gendarme);

  //Creation du controleur
  
  ROSTaskController controleur(&C3P0,&globale);

  //controleur.enableSmoothing(false);

  //controleur.setSmoothRatio(5);

  C3P0.enableControl(true);

  controleur.setTimeLimit(30);

  //Lancement du controleur

  controleur.run();


  return 0;

}
