#include "pr2_head_arms.h"
#include <limits>
#include <iostream>
#include <fstream>

using namespace Eigen;
using namespace std;

int main(int argc, char** argv)
{
  // Init the ROS node
  ros::init(argc, argv, "pr2_joints_record");

  ros::NodeHandle node;

  ros::Rate rate(100.0);
  
  
  VectorXd but(16);

  but << 
    M_PI/4,   0.1,  M_PI/2,  -M_PI/2, -M_PI/2,  -0.05,  0,
   -M_PI/4,  -0.1, -M_PI/2,  -M_PI/2,  M_PI/2,  -0.05,  0,
         0,   0.7;
  
  PR2ArmsHead C3P0;

  C3P0.setState(but);

  C3P0.describeState();

  std::ofstream printer;
  
  printer.open("pr2-joints.dat");

  printer.precision(std::numeric_limits< double >::digits10);


  if(printer){

    while(ros::ok()){
    
      ros::spinOnce();
    
      MyClock::setTime(ros::Time::now().toSec());

      printer << MyClock::getTime() << " ";
      
      printer << C3P0.getState().transpose() << endl;

      rate.sleep();
      
    }

  }

  printer.close();

  return 0;

}
