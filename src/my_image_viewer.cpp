#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <fstream>
#include <limits>
#include <sstream> 
#include <vector>
#include <queue>

typedef std::numeric_limits< double > dbl;

static const std::string OPENCV_WINDOW = "MY VIEW ROS CAM";

static const int max_images = 100;


class ImageConverter
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  
  int num;
  std::string fichierImage;
  std::string fichierTemps; 
  std::ofstream fichier;
  double heure;
  double temps;
  bool history;
  
public:
  ImageConverter(std::string topic, bool record = false):
    it_(nh_)
  {
    history = record;
    num = 10000;
    heure = -1;
    temps = 0 ;
    
    if(history){
   
      fichierTemps = "heure_image.dat";

      fichier.open(fichierTemps.c_str());

      fichier.precision(dbl::digits10);

    }

    // Subscrive to input video feed and publish output video feed
    image_sub_ = it_.subscribe(topic, 10, 
			       &ImageConverter::imageCb, this);
    
    cv::namedWindow(OPENCV_WINDOW);
  }

  ~ImageConverter()
  {
    cv::destroyWindow(OPENCV_WINDOW);
 
  }

  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr;
    try
      {
	cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
      }
    catch (cv_bridge::Exception& e)
      {
	ROS_ERROR("cv_bridge exception: %s", e.what());
	return;
      }

    if(history){

      num++;

      temps = cv_ptr->header.stamp.toSec();

      if( temps > heure){

	heure = temps;

	fichier << heure << " "<< num << std::endl;

	std::ostringstream oss;

	oss << "photo" << num << ".png";

	fichierImage  = oss.str();

	cv::imwrite(fichierImage, cv_ptr->image);

      }

    }

    // Update GUI Window
    cv::imshow(OPENCV_WINDOW, cv_ptr->image);
    cv::waitKey(1);
  }
    
};


int main(int argc, char** argv)
{
  ros::init(argc, argv, "my_image_viewer");
  
  if(argc>1){

    std::string topic(argv[1]);
    ImageConverter ic(topic,argc>2);
    ros::spin();

  }
  else{
    std::cerr << "You should provide one argument to this node \n"
	      << "One example of a correct command is:\n"
	      << "rosrun visual_coordination my_image_viewer /camera_topic"
	      << "\n Put any other argument to enable record of images"
	      << std::endl;
  }
  return 0;
}
