#include "ros_task_controller.h"
#include "pr2_head_arms.h"
#include "tracker_manager.h"
#include "vservoing.h"

using namespace Eigen;
using namespace std;

int main(int argc, char** argv)
{
  // Init the ROS node
  ros::init(argc, argv, "renliw_coord");

  VectorXd but(16);

  but << 
    M_PI/4,  0.1,  M_PI/2,  -M_PI/2, -M_PI/2,  -0.05,  0,
    -M_PI/4, -0.1, -M_PI/2,  -M_PI/2,  M_PI/2,  -0.05,  0,
    0,        0.7;
  
  double lengthCap = 0.075;
  double lengthPen = 0.1;
  double radius = 0.01;
  
  Camera leftEye, rightEye;

  //IntrinsicCam head(772.55,772.55,320.5,240.5);
  IntrinsicCam head(418.6835, 419.16346, 309.05563, 224.1891);
  
  leftEye.setIntrinsicParameters(head);
  rightEye.setIntrinsicParameters(head);
  
  BoundedCylinder cap(lengthCap,radius);
  BoundedCylinder pen(lengthPen,radius);
  
  TrackerManager client_serveur;

  client_serveur.addCamera(&leftEye,"LeftCam");

  client_serveur.addCamera(&rightEye,"RightCam");

  VTracker *  trackerCap = client_serveur.createTracker("cap",&cap);

  VTracker *  trackerPen = client_serveur.createTracker("pen",&pen);

  /*
  BCylinderEstimator capEstimator(&cap,trackerCap);

  BCylinderEstimator penEstimator(&pen,trackerPen);

  // */

  //*

  BCylinderEstimator capEstimator(&cap,&cap);

  BCylinderEstimator penEstimator(&pen,&pen);

  // */

  capEstimator.setRadius(radius);

  penEstimator.setRadius(radius);

  capEstimator.setLength(lengthCap);

  penEstimator.setLength(lengthPen);

  capEstimator.selectInformation(0,0,1,1);

  penEstimator.selectInformation(0,0,1,1);

  //capEstimator.setLogFile("estim-nonlinear.dat");//estimation-cap.dat

  capEstimator.setThreshold(200);

  //penEstimator.setLogFile("estimation-pen.dat");

  penEstimator.setThreshold(200);


  //Gestion des bras du robot

  //Creation des modeles vides et des bras

  GeoRobot brasGauche,brasDroite,couPR2;

  //Chargement du modele geometrique des bras

  loadSE3Robot("/home/rfleurmo/Documents/THESE_LAAS/CODES/C++/robot_models/pr2_gauche.ser",brasGauche);

  loadSE3Robot("/home/rfleurmo/Documents/THESE_LAAS/CODES/C++/robot_models/pr2_droite.ser",brasDroite);

  loadSE3Robot("/home/rfleurmo/Documents/THESE_LAAS/CODES/C++/robot_models/pr2_head.ser",couPR2);

  //Creation des modeles cinematiques des bras

  MyKineModel modeleGauche(&brasGauche);

  MyKineModel modeleDroite(&brasDroite);
  
  MyKineModel modeleTete(&couPR2);

  ManyArmHolderTask tache(true);

  // Defintion des reperes

  Repere epauleGauche(VectRotation(0,0,0),Vector3d(0,  0.188, 0));
  
  Repere epauleDroite(VectRotation(0,0,0),Vector3d(0, -0.188, 0));

  Repere priseGauche(VectRotation(0,0, 0),Vector3d(0.17, 0, -0.07));
  
  Repere priseDroite(VectRotation(0,0, 0),Vector3d(0.17, 0, 0));

  Repere narrowLStereo(VectRotation(0,0,0),Vector3d(0.068, 0.06, 0.115));

  Repere narrowRStereo(VectRotation(0,0,0),Vector3d(0.068, -0.03, 0.115));

  Repere wideLStereo(VectRotation(0,0,0),Vector3d(0.068, 0.03, 0.115));

  Repere wideRStereo(VectRotation(0,0,0),Vector3d(0.068, -0.06, 0.115));

  Repere forearmRCam(VectRotation(3.018, -0.872524,0), Vector3d(0.135-0.321, 0.044, 0));

  Repere identite;

  // Ajout des deux bras a la tache

  tache.addArm(&modeleGauche,epauleGauche);

  tache.addArm(&modeleDroite,epauleDroite);

  tache.addArm(&modeleTete, identite);

  // Defintion des parametres systemes de la tache

  tache.setPeriod(1.0/15);

  double lambda = 0.2;

  double lambdaF = 0.5;

  tache.setLambda(lambda);

  tache.setMinimumErrorNorm(1e-1);

  //Placement des caméras

  tache.addCamera(&rightEye,&modeleTete,wideRStereo,2,"Fixed Head Right Camera");

  tache.addCamera(&leftEye,&modeleTete,wideLStereo,2,"Fixed Head Left camera");

  // Placement des indices visuels

  tache.addVisualTarget(&capEstimator,&modeleGauche,priseGauche,7);

  tache.addVisualTarget(&penEstimator,&modeleDroite,priseDroite,7);

  // Quels indices visuels percoivent les cameras?

  client_serveur.recordCouple(&leftEye,"LeftCam","cap",8);

  client_serveur.recordCouple(&leftEye,"LeftCam","pen",8);

  client_serveur.recordCouple(&rightEye,"RightCam","cap",8);

  client_serveur.recordCouple(&rightEye,"RightCam","pen",8);

  tache.makeTargetSeen(&capEstimator,&leftEye);

  tache.makeTargetSeen(&capEstimator,&rightEye);

  tache.makeTargetSeen(&penEstimator,&leftEye);

  tache.makeTargetSeen(&penEstimator,&rightEye);


  // Quelle est la reference des indices visuels par camera?
  
  penEstimator.attachToCamera(&leftEye);

  //OtherReference ref1(&penEstimator);

  ///tache.giveTargetReference(&leftEye,&capEstimator,&ref1,true);

  penEstimator.attachToCamera(&rightEye);

  OtherReference ref2(&penEstimator);

  tache.giveTargetReference(&rightEye,&capEstimator,&ref2,true);

 
  // Constante arbitraire pour l'écart entre le stylo et le capuchon

  CombinaisonFeature CF(&capEstimator,&penEstimator);

  MatrixXd C1 = MatrixXd::Zero(2,8);

  C1 << 
    0,0,1,0,0,0,0,0,
    0,0,0,0,0,0,1,0;
  
  MatrixXd C2 = MatrixXd::Zero(2,8);

  C2 << 
    0,0, 0,-1,  0,0,0, 0,
    0,0, 0, 0,  0,0,0,-1;
  
  VectorXd K(2);
  
  K << 30, 0;

  CF.setMatrices(C1,C2,K);

  tache.addOtherTask(&rightEye,&CF);

  tache.setState(but);

  /// Ajout de contraintes sur les indices visuels

  ConstraintFeatures capConstraint;

  ConstraintFeatures penConstraint;

  VectorXd sMin = VectorXd::Ones(6);

  VectorXd sMax = VectorXd::Ones(6);

  MatrixXd acti = MatrixXd::Zero(6,8);
  
  sMin = -240.00/sqrt(2) * sMin;

  sMax << 240.00/sqrt(2) * sMax;

  acti.block<3,4>(0,0) << 
    1, 0, 0, 0,
    0, 0, 1, 0,
    0, 0, 0, 1;

  acti.block<3,4>(3,4) = acti.block<3,4>(0,0);

  double coefMax = 0;
  double coefMin = 0.2;
  double alpha = 2;
  double max = 50;


  capConstraint.setCoeffMax(coefMax);
  capConstraint.setCoeffMin(coefMin);
  capConstraint.setAlpha(alpha);
  capConstraint.setMax(max);
  capConstraint.addFeatures(&capEstimator,sMin,sMax,acti);
  
  penConstraint.setCoeffMax(coefMax);
  penConstraint.setCoeffMin(coefMin);
  penConstraint.setAlpha(alpha);
  penConstraint.setMax(max);
  penConstraint.addFeatures(&penEstimator,sMin,sMax,acti);
  

  tache.addOtherTask(&leftEye,&capConstraint);
  tache.addOtherTask(&leftEye,&penConstraint);
  
  tache.addOtherTask(&rightEye,&capConstraint);
  tache.addOtherTask(&rightEye,&penConstraint);
  
    
  //Definition de la premiere tâche a enchainer

  // alignement des deux axes

  MatrixXd selection1 = MatrixXd::Zero(4,34);

  selection1.block<2,2>(0,0) = MatrixXd::Identity(2,2);
  
  selection1.block<2,2>(2,4) = MatrixXd::Identity(2,2);
  
  PartTask alignement(selection1,&tache);

  alignement.setMinimumErrorNorm(1e-2);

  // Distance à conserver

  MatrixXd selection2 = MatrixXd::Zero(1,34);

  selection2(0,8) = 1;

  PartTask distance(selection2,&tache);

  distance.setMinimumErrorNorm(1);

  MyOptimaPriorityTask approche(&distance, &alignement);

  approche.setMinimumErrorNorm(1e-2);

  //Defintion de la deuxieme tache a enchainer
  
  MatrixXd selection3 = MatrixXd::Zero(1,34);

  selection3(0,9) = 1;

  PartTask translation(selection3,&tache);

  translation.setMinimumErrorNorm(1);

  MyOptimaPriorityTask assemblage(&alignement,&translation);

  assemblage.setMinimumErrorNorm(1e-2);

  // Création de la tache spécifique à la contrainte de visibilité

  MatrixXd selection4 = MatrixXd::Zero(24,34);

  selection4.block<24,24>(0,10)  = MatrixXd::Identity(24,24);
  
  PartTask resteVisible(selection4,&tache);

  resteVisible.setMinimumErrorNorm(2);

  resteVisible.setLambda(lambda);

  // Ecriture de la priorite entre les deux taches

  //Definition de la sequence de tâches
  
  SequencyTask sequence(&distance,&approche);

  sequence.addTaskInStack(&assemblage);

  distance.setLambda(AdaptiveGain(lambda,lambdaF));

  alignement.setLambda(AdaptiveGain(lambda,lambdaF));

  translation.setLambda(AdaptiveGain(lambda,lambdaF));
  
  //Gestion des butees

  VectorXd buteeMin(16);

  VectorXd buteeMax(16);

  buteeMin.topRows(7) = brasGauche.getMinimalQ();
  buteeMax.topRows(7) = brasGauche.getMaximalQ();
  
  buteeMin.block<7,1>(7,0) = brasDroite.getMinimalQ();
  buteeMax.block<7,1>(7,0) = brasDroite.getMaximalQ();

  buteeMin.block<2,1>(14,0) = couPR2.getMinimalQ();
  buteeMax.block<2,1>(14,0) = couPR2.getMaximalQ();

  MatrixXd selection5 = MatrixXd::Zero(12,16);

  selection5.block<4,4>(0,0) = MatrixXd::Identity(4,4);
  selection5.block<4,4>(5,7) = MatrixXd::Identity(4,4);
  selection5(4,5)  = 1;
  selection5(9,12) = 1;
  selection5.block<2,2>(10,14) = MatrixXd::Identity(2,2);
  

  buteeMin = selection5*buteeMin;
  buteeMax = selection5*buteeMax;

  QBoundedTask gendarme(buteeMin,buteeMax,selection5);

  gendarme.setCoeffMax(-0.01);

  gendarme.setCoeffMin(0.05);

  gendarme.setAlpha(2);

  gendarme.setMax(10);
  
  gendarme.setState(but);

  gendarme.setLambda(lambda);

  PriorityTask etage1(&resteVisible,&sequence);

  etage1.setState(but);

  etage1.hideTaskOnControl(&resteVisible);

  PriorityTask globale(&gendarme,&etage1);

  globale.setState(but);

  
  /// CONNECTION AU ROBOT PR2
  
  PR2ArmsHead C3P0;

  //C3P0.setState(0.0 * but);

  C3P0.setState(but);

  C3P0.describeState();
  
  //Creation du controleur
  
  ROSTaskController controleur(&C3P0,&globale);

  //controleur.enableSmoothing(false);

  //controleur.setSmoothRatio(5);

  C3P0.enableControl(false);

  controleur.setTimeLimit(180);

  //Lancement du controleur

  controleur.run();

  return 0;

}
