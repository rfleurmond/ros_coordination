#include "ros_task_controller.h"
#include "ros_robot_system.h"
#include "pr2_head_arms.h"
#include "tracker_manager.h"
#include "my_ros_camera.h"
#include "complex_tracker.h"
#include "vservoing.h"

using namespace Eigen;
using namespace std;

int main(int argc, char** argv)
{
  // Init the ROS node
  ros::init(argc, argv, "renliw_coord");

  ros::NodeHandle node;
  
  VectorXd but(16);

  but << 
    M_PI/4,   0.1,  M_PI/2,  -M_PI/2, -M_PI/2,  -0.05,  0,
    -M_PI/4, -0.1, -M_PI/2,  -M_PI/2,  M_PI/2,  -0.05,  0,
    0,        0.7;
  
  double lengthCap = 0.075;
  double lengthPen = 0.1;
  double radius = 0.01;
  
  double r2d = 360/M_PI;
   
  IntrinsicCam headRight(417.66329, 417.88768, 318.43842, 221.30017);
  IntrinsicCam headLeft( 421.96246, 422.28643, 317.76621, 224.33886);
  IntrinsicCam rightArm( 430.67783, 430.19755, 324.72594, 236.54259);
  IntrinsicCam leftArm(  424.97353, 427.11019, 324.51727, 228.79747);
 
  MyRosCamera rightEye(node,"RightEye","/wide_stereo/right/image_raw",headRight);
  MyRosCamera leftEye(node,"LeftEye","/wide_stereo/left/image_raw",headLeft);
  MyRosCamera camRarm(node,"RArmEye","/r_forearm_cam/image_raw",rightArm);
  
  
  BoundedCylinder cap(lengthCap,radius);
  BoundedCylinder pen(lengthPen,radius);
  
  CylinderTracker trackRightCap;
  CylinderTracker trackRightPen;
  CylinderTracker trackLeftCap;
  CylinderTracker trackLeftPen;
  CylinderTracker trackArmCap;

  
  ComplexTracker capTracker("cap",&trackRightCap);
  capTracker.addInternalTracker(&rightEye,&trackRightCap);
  capTracker.addInternalTracker(&leftEye,&trackLeftCap);
  capTracker.addInternalTracker(&camRarm,&trackArmCap);
  
  ComplexTracker penTracker("pen",&trackRightPen);
  penTracker.addInternalTracker(&rightEye,&trackRightPen);
  penTracker.addInternalTracker(&leftEye,&trackLeftPen);
  
  
  /*
  BCylinderEstimator capEstimator(&cap,&capTracker);

  BCylinderEstimator penEstimator(&pen,&penTracker);

  // */

  //*

  BCylinderEstimator capEstimator(&cap,&cap);

  BCylinderEstimator penEstimator(&pen,&pen);

  // */

  capEstimator.setRadius(radius);

  penEstimator.setRadius(radius);

  capEstimator.setLength(lengthCap);

  penEstimator.setLength(lengthPen);

  capEstimator.selectInformation(0,0,1,1);

  penEstimator.selectInformation(0,0,1,1);

  capEstimator.setLogFile("estimation-cap.dat");

  capEstimator.setThreshold(5);

  penEstimator.setLogFile("estimation-pen.dat");

  penEstimator.setThreshold(5);

  //Gestion des bras du robot

  //Creation des modeles vides et des bras

  GeoRobot brasGauche,brasDroite,couPR2;

  //Chargement du modele geometrique des bras

  loadSE3Robot("/home/rfleurmo/Documents/THESE_LAAS/CODES/C++/robot_models/pr2_gauche.ser",brasGauche);

  loadSE3Robot("/home/rfleurmo/Documents/THESE_LAAS/CODES/C++/robot_models/pr2_droite.ser",brasDroite);

  loadSE3Robot("/home/rfleurmo/Documents/THESE_LAAS/CODES/C++/robot_models/pr2_head.ser",couPR2);


  //Creation des modeles cinematiques des bras

  MyKineModel modeleGauche(&brasGauche);

  MyKineModel modeleDroite(&brasDroite);

  MyKineModel modeleTete(&couPR2);


  ManyArmHolderTask tache(true);

  // Defintion des reperes

  Repere epauleGauche(VectRotation(0,0,0),Vector3d(0,  0.188, 0));
  
  Repere epauleDroite(VectRotation(0,0,0),Vector3d(0, -0.188, 0));

  Repere priseGauche(VectRotation(0, 0, 0),Vector3d(0.17, 0, 0));
  
  Repere priseDroite(VectRotation(0, 0, 0),Vector3d(0.17, 0, 0.00));

  Repere narrowLStereo(VectRotation(0,0,0),Vector3d(0.068, 0.06, 0.115));

  Repere narrowRStereo(VectRotation(0,0,0),Vector3d(0.068, -0.03, 0.115));
  
  Repere wideLStereo(VectRotation(0,0,0),Vector3d(0.068, 0.03, 0.115));

  Repere wideRStereo(VectRotation(0,0,0),Vector3d(0.068, -0.06, 0.115));

  Repere forearmRCam(VectRotation(3.018, -0.872524,0),
		     Vector3d(0.135-0.321, 0.044, 0));

  Repere forearmLCam(VectRotation(0,0,-0.562866),
		     Vector3d(0.135-0.321, 0.044, 0));

  Repere identite;

  // Ajout des deux bras a la tache

  tache.addArm(&modeleGauche,epauleGauche);

  tache.addArm(&modeleDroite,epauleDroite);

  tache.addArm(&modeleTete, identite);

  // Defintion des parametres systemes de la tache

  tache.setPeriod(1.0/15);

  double lambda = 0.2;

  double lambdaF = 0.5;

  tache.setLambda(lambda);

  tache.setMinimumErrorNorm(1e-2);

  //Placement des caméras

  tache.addCamera(&rightEye,&modeleTete,wideRStereo,2,"Fixed Head Right Camera");

  tache.addCamera(&leftEye,&modeleTete,wideLStereo,2,"Fixed Head Left camera");

  tache.addCamera(&camRarm,&modeleDroite,forearmRCam,5,"Mobile Right Camera");

  // Placement des indices visuels

  tache.addVisualTarget(&penEstimator,&modeleGauche,priseGauche,7);

  tache.addVisualTarget(&capEstimator,&modeleDroite,priseDroite,7);

  // Quels indices visuels percoivent les cameras?

  tache.makeTargetSeen(&capEstimator,&leftEye);

  tache.makeTargetSeen(&capEstimator,&rightEye);

  tache.makeTargetSeen(&penEstimator,&leftEye);

  tache.makeTargetSeen(&penEstimator,&rightEye);

  tache.makeTargetSeen(&capEstimator,&camRarm);

  // Quelle est la reference des indices visuels par camera?
  
  penEstimator.attachToCamera(&leftEye);

  penEstimator.attachToCamera(&rightEye);

  OtherReference ref2(&penEstimator);

  tache.giveTargetReference(&rightEye,&capEstimator,&ref2,true);

  //tache.giveKandCmatrix(&rightEye,&capEstimator,MatrixXd::Identity(8,8),VectorXd::Ones(8));

  
  /// CONNECTION AU ROBOT PR2
  
  PR2ArmsHead C3P0;

  C3P0.enableControl(false);

  C3P0.setState(but);

  C3P0.describeState();

  rightEye.addFollowers(&trackRightCap);
  rightEye.addFollowers(&trackRightPen);
  leftEye.addFollowers(&trackLeftCap);
  leftEye.addFollowers(&trackLeftPen);
  camRarm.addFollowers(&trackArmCap);
  

  tache.setState(C3P0.getState());
  
  
  /// VERIFICATION DE CERTAINES VALEURS

  VectorXd val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  MatrixXd J = tache.jacobien(but);

  cout <<"Jacobien de la tache\n"<< J <<endl;

  FullPivLU<MatrixXd> lu_decomp(J);

  cout << "The rank of J is " << lu_decomp.rank() << endl;

  cout <<"Etat du systeme\n"<<but<<endl;

  val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  
  //Creation du controleur
  
  ROSTaskController controleur(&C3P0,&tache);

  //controleur.enableSmoothing(false);

  //controleur.setSmoothRatio(5);

  controleur.setTimeLimit(100);

  //Lancement du controleur

  controleur.run();


  return 0;

}
