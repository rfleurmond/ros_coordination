#include "ros_task_controller.h"
#include "ros_robot_system.h"
#include "tracker_manager.h"
#include "vservoing.h"

using namespace Eigen;
using namespace std;

int main(int argc, char** argv)
{
  // Init the ROS node
  ros::init(argc, argv, "renliw_coord");

  VectorXd but(14);

  but << 
    M_PI/4,  0.1,  M_PI/2,  -M_PI/2, -M_PI/2,  -0.05,  0,
    -M_PI/4, -0.1, -M_PI/2,  -M_PI/2,  M_PI/2,  -0.05,  0;
  
  //but = VectorXd::Zero(14);

  double LC = 0.075;
  double LP = 0.1;
  double R = 0.01;
  
  Camera leftEye, rightEye;

  //IntrinsicCam head(772.55,772.55,320.5,240.5);
  IntrinsicCam head(418.6835, 419.16346, 309.05563, 224.1891);
  
  leftEye.setIntrinsicParameters(head);
  rightEye.setIntrinsicParameters(head);
  
  
  BoundedCylinder cap(LC,R);
  BoundedCylinder pen(LP,R);
  

  TrackerManager client_serveur;

  client_serveur.addCamera(&leftEye,"LeftCam");

  client_serveur.addCamera(&rightEye,"RightCam");

  VTracker *  trackerCap = client_serveur.createTracker("cap",&cap);

  VTracker *  trackerPen = client_serveur.createTracker("pen",&pen);

  /*
  BCylinderEstimator capEstimator(&cap,trackerCap);

  BCylinderEstimator penEstimator(&pen,trackerPen);

  // */

  //*

  BCylinderEstimator capEstimator(&cap,&cap);

  BCylinderEstimator penEstimator(&pen,&pen);

  // */

  capEstimator.setRadius(R);

  penEstimator.setRadius(R);

  capEstimator.setLength(LC);

  penEstimator.setLength(LP);

  capEstimator.selectInformation(0,0,1,1);

  penEstimator.selectInformation(0,0,1,1);

  capEstimator.setLogFile("estimation-cap.dat");

  capEstimator.setThreshold(50);

  penEstimator.setLogFile("estimation-pen.dat");

  penEstimator.setThreshold(50);



  //Gestion des bras du robot

  //Creation des modeles vides et des bras

  GeoRobot brasGauche,brasDroite;

  //Chargement du modele geometrique des bras

  loadSE3Robot("/home/rfleurmo/Documents/THESE_LAAS/CODES/C++/robot_models/pr2_gauche.ser",brasGauche);

  loadSE3Robot("/home/rfleurmo/Documents/THESE_LAAS/CODES/C++/robot_models/pr2_droite.ser",brasDroite);

  //Creation des modeles cinematiques des bras

  MyKineModel modeleGauche(&brasGauche);

  MyKineModel modeleDroite(&brasDroite);
  

  ManyArmHolderTask tache(true);

  // Defintion des reperes

  Repere tigeGauche(VectRotation(0,0,0),Vector3d(-3*LC, 0, 0));
  
  Repere tigeDroite(VectRotation(0,0,0),Vector3d(-3*LC, 0, 0));

  Repere epauleGauche(VectRotation(0,0,0),Vector3d(0,  0.188, 0));
  
  Repere epauleDroite(VectRotation(0,0,0),Vector3d(0, -0.188, 0));

  Repere priseGauche(VectRotation(0, 0, 0),Vector3d(0.17, 0,-LC));
  
  Repere priseDroite(VectRotation(0, 0, 0),Vector3d(0.17, 0, 0.00));

  Repere camGauche(VectRotation(0,M_PI/4,0),Vector3d(0.067, 0.029, 0.4968));

  Repere camDroite(VectRotation(0,M_PI/4,0),Vector3d(0.067,-0.060, 0.4968));

  Repere fixDroite(VectRotation(3.018, -0.872524,0),
		   Vector3d(0.135-0.321, 0.044, 0));

  Repere simple;

  // Ajout des deux bras a la tache

  tache.addArm(&modeleGauche,epauleGauche);

  tache.addArm(&modeleDroite,epauleDroite);

  // Defintion des parametres systemes de la tache

  tache.setPeriod(1.0/20);

  double lambda = 0.2;

  double lambdaF = 0.5;

  tache.setLambda(lambda);

  tache.setMinimumErrorNorm(1e-2);

  //Placement des caméras

  tache.addCamera(&rightEye,NULL,camDroite,7,"Fixed Head right Camera");

  tache.addCamera(&leftEye,NULL,camGauche,7,"Fixed Head Left camera");

  // Placement des indices visuels

  tache.addVisualTarget(&capEstimator,&modeleGauche,priseGauche,7);

  tache.addVisualTarget(&penEstimator,&modeleDroite,priseDroite,7);

  // Quels indices visuels percoivent les cameras?

  client_serveur.recordCouple(&leftEye,"LeftCam","cap",8);

  client_serveur.recordCouple(&leftEye,"LeftCam","pen",8);

  client_serveur.recordCouple(&rightEye,"RightCam","cap",8);

  client_serveur.recordCouple(&rightEye,"RightCam","pen",8);

  tache.makeTargetSeen(&capEstimator,&leftEye);

  tache.makeTargetSeen(&capEstimator,&rightEye);

  tache.makeTargetSeen(&penEstimator,&leftEye);

  tache.makeTargetSeen(&penEstimator,&rightEye);


  // Quelle est la reference des indices visuels par camera?
  
  penEstimator.attachToCamera(&leftEye);

  //OtherReference ref1(&penEstimator);

  ///tache.giveTargetReference(&leftEye,&capEstimator,&ref1,true);

  penEstimator.attachToCamera(&rightEye);

  OtherReference ref2(&penEstimator);

  tache.giveTargetReference(&rightEye,&capEstimator,&ref2,true);

 

 // Constante arbitraire pour l'écart entre le stylo et le capuchon

  CombinaisonFeature CF(&capEstimator,&penEstimator);

  MatrixXd C1 = MatrixXd::Zero(2,8);

  C1 << 
    0,0,1,0,0,0,0,0,
    0,0,0,0,0,0,1,0;
  
  MatrixXd C2 = MatrixXd::Zero(2,8);

  C2 << 
    0,0, 0,-1,  0,0,0, 0,
    0,0, 0, 0,  0,0,0,-1;
  
  VectorXd K(2);
  
  K << 30, 0;

  CF.setMatrices(C1,C2,K);

  tache.addOtherTask(&rightEye,&CF);

  tache.setState(but);
  
  //Defintion de la premiere tâche a enchainer

  // alignement des deux axes

  MatrixXd selection1 = MatrixXd::Zero(4,10);

  selection1.block<2,2>(0,0) = MatrixXd::Identity(2,2);
  
  selection1.block<2,2>(2,4) = MatrixXd::Identity(2,2);
  
  PartTask alignement(selection1,&tache);

  alignement.setMinimumErrorNorm(1e-1);

  // Distance à conserver

  MatrixXd selection2 = MatrixXd::Zero(1,10);

  selection2(0,8) = 1;

  PartTask distance(selection2,&tache);

  distance.setMinimumErrorNorm(1e-1);

  PriorityTask approche(&distance, &alignement);

  approche.setMinimumErrorNorm(1e-1);

  //Defintion de la deuxieme tache a enchainer
  
  MatrixXd selection3 = MatrixXd::Zero(1,10);

  selection3(0,9) = 1;

  PartTask translation(selection3,&tache);

  translation.setMinimumErrorNorm(1e-1);

  PriorityTask assemblage(&alignement,&translation);

  assemblage.setMinimumErrorNorm(1e-1);

  // Ecriture de la priorite entre les deux taches


  //Definition de la sequence de tâches
  
  SequencyTask sequence(&distance,&approche);

  sequence.addTaskInStack(&assemblage);

  distance.setLambda(AdaptiveGain(lambda,lambdaF));

  approche.setLambda(AdaptiveGain(lambda,lambdaF));

  assemblage.setLambda(AdaptiveGain(lambda,lambdaF));
  
  //Gestion des butees

  VectorXd buteeMin(14);

  VectorXd buteeMax(14);

  buteeMin.topRows(7) = brasGauche.getMinimalQ();
  buteeMin.bottomRows(7) = brasDroite.getMinimalQ();

  buteeMax.topRows(7) = brasGauche.getMaximalQ();
  buteeMax.bottomRows(7) = brasDroite.getMaximalQ();

  MatrixXd selection5 = MatrixXd::Zero(10,14);

  selection5.block<4,4>(0,0) = MatrixXd::Identity(4,4);
  selection5.block<4,4>(5,7) = MatrixXd::Identity(4,4);
  selection5(4,5)  = 1;
  selection5(9,12) = 1;

  buteeMin = selection5 * buteeMin;
  buteeMax = selection5 * buteeMax;

  QBoundedTask gendarme(buteeMin,buteeMax,selection5);

  gendarme.setCoeffMax(-0.01);

  gendarme.setCoeffMin(0.05);

  gendarme.setAlpha(2);

  gendarme.setMax(10);
  
  gendarme.setState(but);

  gendarme.setLambda(lambda);

  /// CONNECTION AU ROBOT PR2
  
  PR2Arms C3P0;

  C3P0.setState(but);
  
  /// VERIFICATION DE CERTAINES VALEURS

  VectorXd val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  MatrixXd J = tache.jacobien(but);

  cout <<"Jacobien de la tache\n"<< J <<endl;

  FullPivLU<MatrixXd> lu_decomp(J);

  cout << "The rank of J is " << lu_decomp.rank() << endl;

  cout <<"Etat du systeme\n"<<but<<endl;

  val = tache.valeur();

  cout << "La fonction de tache \n"<<val<<endl;

  MyOptimaPriorityTask globale(&gendarme,&sequence);

  globale.setState(but);

  //globale.hideTaskOnControl(&gendarme);

  //Creation du controleur
  
  ROSTaskController controleur(&C3P0,&globale);

  //controleur.enableSmoothing(false);

  //controleur.setSmoothRatio(5);

  C3P0.enableControl(true);

  controleur.setTimeLimit(180);

  //Lancement du controleur

  controleur.run();


  return 0;

}
