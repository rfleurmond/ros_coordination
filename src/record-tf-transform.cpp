#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <string>
#include <fstream>
#include "outils.h"
#include <limits>


typedef std::numeric_limits< double > dbl;

int main(int argc, char** argv){

  ros::init(argc, argv, "my_tf_listener");

  ros::NodeHandle node;

  tf::TransformListener listener;

  std::string chemin = "pose_camera.dat";
  
  std::ofstream fichier(chemin.c_str());

  fichier.precision(dbl::digits10);

  double heure = -1;

  double temps = 0;

  ros::Rate rate(200.0);

  while (node.ok()){

    tf::StampedTransform transform;

    tf::Quaternion q;

    try{
      listener.lookupTransform("/base_link", "/r_forearm_cam_frame",
                               ros::Time(0), transform);
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s",ex.what());
    }

    temps = transform.stamp_.toSec();

    if( temps > heure){

      heure = temps;

      fichier << heure;

      q = transform.getRotation();

      fichier << " " << q.x() << " " << q.y() << " " << q.z() << " " << q.w() ;

      fichier << " " << transform.getOrigin().x() << " " << transform.getOrigin().y() << " " << transform.getOrigin().z() << std::endl;
      
    }
    
    rate.sleep();

  }

  fichier.close();
  return 0;
};
