#include <ros/ros.h>
#include <control_msgs/JointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include "Repere.h"

typedef actionlib::SimpleActionClient< control_msgs::JointTrajectoryAction > TrajClient;

class RobotArm
{
private:
  // Action client for the joint trajectory action 
  // used to trigger the arm movement action
  TrajClient* traj_clientL_;
  TrajClient* traj_clientR_;
  Eigen::VectorXd but;

public:
  //! Initialize the action client and wait for action server to come up
  RobotArm(Eigen::VectorXd goal) 
  {
    // tell the action client that we want to spin a thread by default
    traj_clientL_ = new TrajClient("l_arm_controller/joint_trajectory_action", true);
    traj_clientR_ = new TrajClient("r_arm_controller/joint_trajectory_action", true);

    but = goal;

    // wait for action server to come up
    while(!traj_clientL_->waitForServer(ros::Duration(5.0))){
      ROS_INFO("Waiting for the joint_trajectory_action server");
    }
    while(!traj_clientR_->waitForServer(ros::Duration(5.0))){
      ROS_INFO("Waiting for the joint_trajectory_action server");
    }
  }

  //! Clean up the action client
  ~RobotArm()
  {
    delete traj_clientL_;
    delete traj_clientR_;
  }

  //! Sends the command to start a given trajectory
  void startTrajectory(control_msgs::JointTrajectoryGoal goalL, control_msgs::JointTrajectoryGoal goalR)
  {
    // When to start the trajectory: 1s from now
    goalL.trajectory.header.stamp = ros::Time::now() + ros::Duration(1.0);
    goalR.trajectory.header.stamp = goalL.trajectory.header.stamp;
    traj_clientL_->sendGoal(goalL);
    traj_clientR_->sendGoal(goalR);
  }

  //! Generates a simple trajectory with two waypoints, used as an example
  /*! Note that this trajectory contains two waypoints, joined together
      as a single trajectory. Alternatively, each of these waypoints could
      be in its own trajectory - a trajectory can have one or more waypoints
      depending on the desired application.
  */
  control_msgs::JointTrajectoryGoal armExtensionTrajectoryL()
  {
    //our goal variable
    control_msgs::JointTrajectoryGoal goal;

    // First, the joint names, which apply to all waypoints
    goal.trajectory.joint_names.push_back("l_shoulder_pan_joint");
    goal.trajectory.joint_names.push_back("l_shoulder_lift_joint");
    goal.trajectory.joint_names.push_back("l_upper_arm_roll_joint");
    goal.trajectory.joint_names.push_back("l_elbow_flex_joint");
    goal.trajectory.joint_names.push_back("l_forearm_roll_joint");
    goal.trajectory.joint_names.push_back("l_wrist_flex_joint");
    goal.trajectory.joint_names.push_back("l_wrist_roll_joint");

    // We will have two waypoints in this goal trajectory
    goal.trajectory.points.resize(1);

    // First trajectory point
    // Positions
    int ind = 0;
    
    // To be reached 1 second after starting along the trajectory
    goal.trajectory.points[ind].time_from_start = ros::Duration(1.0);

    // trajectory point
    // Positions
    goal.trajectory.points[ind].positions.resize(7);
    for (size_t j = 0; j < 7; ++j){
      goal.trajectory.points[ind].positions[j] = but(j);
    }
    
    
    // Velocities
    goal.trajectory.points[ind].velocities.resize(7);
    for (size_t j = 0; j < 7; ++j){
      goal.trajectory.points[ind].velocities[j] = 0.0;
    }
    //we are done; return the goal
    return goal;
  }

  control_msgs::JointTrajectoryGoal armExtensionTrajectoryR()
  {
    //our goal variable
    control_msgs::JointTrajectoryGoal goal;

    // First, the joint names, which apply to all waypoints
    goal.trajectory.joint_names.push_back("r_shoulder_pan_joint");
    goal.trajectory.joint_names.push_back("r_shoulder_lift_joint");
    goal.trajectory.joint_names.push_back("r_upper_arm_roll_joint");
    goal.trajectory.joint_names.push_back("r_elbow_flex_joint");
    goal.trajectory.joint_names.push_back("r_forearm_roll_joint");
    goal.trajectory.joint_names.push_back("r_wrist_flex_joint");
    goal.trajectory.joint_names.push_back("r_wrist_roll_joint");
    
    // We will have two waypoints in this goal trajectory
    goal.trajectory.points.resize(1);

    // First trajectory point
    // Positions
    int ind = 0;
    // To be reached 1 second after starting along the trajectory
    goal.trajectory.points[ind].time_from_start = ros::Duration(1.0);

    // Second trajectory point
    // Positions
    goal.trajectory.points[ind].positions.resize(7);
    for (size_t j = 0; j < 7; ++j){
      goal.trajectory.points[ind].positions[j] = but(j+7);
    }
    
    // Velocities
    goal.trajectory.points[ind].velocities.resize(7);
    for (size_t j = 0; j < 7; ++j){
      goal.trajectory.points[ind].velocities[j] = 0.0;
    }
    
    //we are done; return the goal
    return goal;
  }

  //! Returns the current state of the action
  actionlib::SimpleClientGoalState getStateL()
  {
    return traj_clientL_->getState();
  }

  actionlib::SimpleClientGoalState getStateR()
  {
    return traj_clientR_->getState();
  }
 
};

int main(int argc, char** argv)
{
  // Init the ROS node
  ros::init(argc, argv, "robot_driver");
  
  Eigen::VectorXd but(14);

  if(argc==15){
    for(int i = 0; i < 14; i++){
      but(i) = atof(argv[i+1]);
    }

    RobotArm arms(but);
    // Start the trajectory
    arms.startTrajectory(arms.armExtensionTrajectoryL(),arms.armExtensionTrajectoryR());
    // Wait for trajectory completion
    while(!arms.getStateL().isDone() && !arms.getStateR().isDone() && ros::ok())
      {
	usleep(50000);
      }
  }
}
