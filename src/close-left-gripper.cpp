#include "gripper.h"

int main(int argc, char** argv){

  ros::init(argc, argv, "closeLeft");

  Gripper gripper(Gripper::LEFT_GRIPPER);

  gripper.close();

  return 0;
}

