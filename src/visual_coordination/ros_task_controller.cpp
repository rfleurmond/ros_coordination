#include "ros_task_controller.h"
#include <fstream>
#include <stdlib.h>
#include <time.h>

using namespace std;
using namespace Eigen;

ROSTaskController::ROSTaskController(TaskSystem * sys, Task * ta):
  TaskController(sys,ta)
{
  //hasToLog = false;
}


void ROSTaskController::run(){
  
  ofstream printerT,printerS;
  bool accesT = false;
  bool accesS = false;
  
  if(hasToLog){
    printerT.open(fichierTache.c_str());
    if(printerT){
      accesT = true;
    }
    else{
      cout<<"[RosTaskController]: Warn: Log file for task not opened"<<endl;
    }
    printerS.open(fichierSys.c_str());
    if(printerS){
      accesS = true;
    }
    else{
      cout<<"[RosTaskController]: Warn: Log file for system  not opened"<<endl;
    }
    
  }
  
  cout<<"[RosTaskController]: Controller starting .........."<<endl;

  double duree = 0;

  double interval = 0;
 
  ros::Rate loop_rate(100);

  MyClock::start(ros::Time::now().toSec());

  MyClock::setTime(ros::Time::now().toSec());

  heure = MyClock::getTime();

  double anniv = heure;
    
  while(ros::ok()){

    MyClock::setTime(ros::Time::now().toSec());

    if(MyClock::getTime() - heure >= periodT)

      {

	heure = MyClock::getTime();

	tache->setState(etat_q);
    
	erreur = tache->valeur();

	getTimeCommand(heure);

	//cout << "[RosTaskController]: Time(sec) = "<<heure<<endl;
    
	tache->updateGain();
    
	tache->recordCommand(commande);

	systeme->takeCommand(commande);

	MyClock::setTime(ros::Time::now().toSec());

	heure = MyClock::getTime();

	systeme->updateTime(heure);

	tache->updateTime(heure);

	etat_q = systeme->getState();

      }

    etat_q = systeme->getState();

    anniv = MyClock::getTime();

    if(accesT){
      printerT<<anniv;
      printerT<<" "<<erreur.transpose()<<endl;
    }
    if(accesS){
      printerS<<anniv;
      printerS<<" "<<commande.transpose();
      printerS<<" "<<etat_q.transpose()<<endl;
    }

    if(heure>=horizon || tache->isComplete()){
      break;
    }

    ros::spinOnce();
    
    //loop_rate.sleep();
     
  }
  
  if(heure>=horizon){
    cout<<"[RosTaskController]: Delay expired :  "<<heure<<" >= "<<horizon<<endl;
  }
  else{
    cout<<"[RosTaskController]: Task is completed at  : "<<heure<<endl;
  }

  if(accesT){
    printerT<<heure;
    printerT<<" "<<erreur.transpose()<<endl;
    printerT.close();
  }
  if(accesS){
    printerS<<heure;
    printerS<<" "<<commande.transpose();
    printerS<<" "<<etat_q.transpose()<<endl;
    printerS.close();
  }
      
  cout<<"[RosTaskController]: Controller normally stopped at : "<<heure <<endl;
}

