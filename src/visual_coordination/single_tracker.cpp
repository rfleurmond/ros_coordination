#include "single_tracker.h"

using namespace Eigen;
using namespace std;

SingleTracker::SingleTracker(string topic, string window, string camera, string target):
  itr(node),
  topicName(topic),
  windowName(window),
  myDisplay(window),
  cameraName(camera),
  targetName(target)
{
  image_sub = itr.subscribe(topicName,5,&SingleTracker::imageCallback,this);

  feat_sub = node.subscribe(camera+"_"+target+"_reset",100,&SingleTracker::resetCallback,this);

  feat_pub = node.advertise<visual_coordination::MFeatures>(camera+"_"+target, 1000);

  myDisplay.ouvrirFenetre();
}

SingleTracker::~SingleTracker(){
  myDisplay.fermerFenetre();
}


void SingleTracker::imageCallback(const sensor_msgs::ImageConstPtr& msg){

  cv_bridge::CvImagePtr cv_ptr;
  
  try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
  catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

  frameId = cv_ptr->header.frame_id;
    
  lastImage = cv::Mat(cv_ptr->image);

  realTrack();

  publishVisualCues(S);
   
} 


void SingleTracker::resetCallback(const visual_coordination::MFeaturesConstPtr & msg){

  VectorXd SS(msg->length);

  assert(msg->length == msg->data.size());

  for(int i = 0; i < msg->length; i++){
    SS(i) = msg->data[i];
  }

  resetTracker(SS);

}

  
  

void SingleTracker::publishVisualCues(const VectorXd & SS){  

  visual_coordination::MFeatures msg;
  
  msg.camera_name = cameraName;

  msg.target_name = targetName;

  msg.lost = 0;
  
  msg.length = SS.rows();

  for(int i = 0; i < msg.length; i++){
    msg.data.push_back(SS(i));
  }  

  msg.header.frame_id = frameId;

  msg.header.stamp = ros::Time::now();
    
  feat_pub.publish(msg);

}

void SingleTracker::realTrack(){

  // Update GUI Window
  myDisplay.setImage(lastImage);
  myDisplay.affichage();
  cv::waitKey(1);

  S = VectorXd::Zero(4);
  S << 1, 9, 8, 5;
}

void SingleTracker::resetTracker(const VectorXd & SS){

}

