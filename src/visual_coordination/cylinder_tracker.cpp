#include "cylinder_tracker.h"

using namespace Eigen;
using namespace std;

CylTracker::CylTracker(string topic, string window, string camera, string target):
  SingleTracker(topic,window,camera,target),
  glasses()
{
  IntrinsicCam optique(418.6835, 419.16346, 309.05563, 224.1891); // Bob  wide left stereo
  
  eye.setIntrinsicParameters(optique);

  myDisplay.setOptique(optique);

  glasses.attachToCamera(&eye);
  
  glasses.initTracking(&myDisplay);
 
}

void CylTracker::realTrack(){

  //glasses.track(lastImage);
  
  S =  glasses.getFunction();

  myDisplay.setImage(lastImage);

  glasses.draw(&myDisplay,cv::Scalar(0,255,255));

  myDisplay.affichage();

  cv::waitKey(1);


}

void CylTracker::resetTracker(const VectorXd & SS){
  glasses.findFeatures(SS);
}
