#include "ros_robot_system.h"

using namespace Eigen;
using namespace std;
 
PR2Arms::PR2Arms():
  armDOF(7),
  totalDOF(14),
  K(3),
  period(0.05),
  deadline(0.05),
  waitFor(false),
  controllable(false),
  node(),
  state(VectorXd::Zero(14)),
  velocity(VectorXd::Zero(14)),
  control(VectorXd::Zero(14)),
  prediction(VectorXd::Zero(14)),
  joint_names(vector<string>()),
  left_arm_client(NULL),
  right_arm_client(NULL)
{

  sub_joint = node.subscribe("/joint_states",100,&PR2Arms::receiveCurrentState,this);

  left_arm_client = new TrajClient("l_arm_controller/joint_trajectory_action", true);
  right_arm_client = new TrajClient("r_arm_controller/joint_trajectory_action", true);

  // wait for action server to come up
  while(!left_arm_client->waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the left joint_trajectory_action server");
  }
  while(!right_arm_client->waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the right joint_trajectory_action server");
  }

  std::cout<<"Connection made in class PR2Arms !"<<std::endl;

  joint_names.push_back("l_shoulder_pan_joint");
  joint_names.push_back("l_shoulder_lift_joint");
  joint_names.push_back("l_upper_arm_roll_joint");
  joint_names.push_back("l_elbow_flex_joint");
  joint_names.push_back("l_forearm_roll_joint");
  joint_names.push_back("l_wrist_flex_joint");
  joint_names.push_back("l_wrist_roll_joint");
  
  joint_names.push_back("r_shoulder_pan_joint");
  joint_names.push_back("r_shoulder_lift_joint");
  joint_names.push_back("r_upper_arm_roll_joint");
  joint_names.push_back("r_elbow_flex_joint");
  joint_names.push_back("r_forearm_roll_joint");
  joint_names.push_back("r_wrist_flex_joint");
  joint_names.push_back("r_wrist_roll_joint");
  
}

PR2Arms::~PR2Arms(){
  delete left_arm_client;
  delete right_arm_client;
}


int PR2Arms::getDimState() const{
  return totalDOF;
}

VectorXd PR2Arms::getState() const{
  return state;
}

void PR2Arms::setState(const VectorXd & etat){
  assert(etat.rows()==totalDOF);
  control = VectorXd::Zero(totalDOF);
  prediction = etat;
  deadline = 2.0;
  K = 3;
  waitFor = true;
  retrieveCurrentState();
  pushTrajectory();
  retrieveCurrentState();
}

void PR2Arms::takeCommand(const VectorXd & order){
  assert(order.rows()==totalDOF);
  control = order;
  prediction.noalias() = state + control * period;
  deadline = period;
  K = 100;
  waitFor = false;
  if(controllable){
    pushTrajectory();
  }
}

void PR2Arms::updateTime(double t){

}

void PR2Arms::enableControl(bool choice){
  controllable = choice;
}  

double PR2Arms::getPeriod() const{
  return period;
}

void PR2Arms::receiveCurrentState(const sensor_msgs::JointStateConstPtr & state_msg){

  string nom;
  string nombre;
  int depart = 0;
  int index = 0;
  int j = 0;
  int k = 0;
  int taille = 0;

  taille = state_msg->position.size();
  
  //extract the joint angles from it
  depart = 0;
  for(int i = 0; i < totalDOF; i++){
    nom = joint_names[i];
    nombre = state_msg->name[index];
    j = 0;
    while(j < taille){
      k = (j%2==0)? j/2 : -j/2;
      index = (depart + taille + k) % taille;
      nombre = state_msg->name[index];
      j++;
      if(nombre == nom){
	//std::cout <<nom <<" --- J = " << j <<"/"<<taille<<std::endl;
	state(i) = state_msg->position[index];
	velocity(i) = state_msg->velocity[index];
	depart = (index+1)%taille;
	break;
      }
    }
  }

  //std::cout << "Etat actuel =\n" << velocity.transpose() <<std::endl;


}

void PR2Arms::retrieveCurrentState(){
  //get a single message from the topic '/joint_states'
  sensor_msgs::JointStateConstPtr state_msg =  
    ros::topic::waitForMessage<sensor_msgs::JointState>("/joint_states");

  receiveCurrentState(state_msg);

}


void PR2Arms::pushTrajectory(){

  
  /// COMPUTE THE LEFT ARM TRAJECTORY

  //std::cout << "Nouvel etat a atteindre=\n" << prediction.transpose() <<std::endl;

  control_msgs::JointTrajectoryGoal leftGoal;

  for(int i = 0; i < armDOF; i++){
    leftGoal.trajectory.joint_names.push_back(joint_names[i]);
  }

  leftGoal.trajectory.points.resize(K);

  
  for(int i = 0; i < K; i++){
  
    // To be reached 1 second after starting along the trajectory
    leftGoal.trajectory.points[i].time_from_start = ros::Duration(i*deadline);
    
    // trajectory point
    // Positions
    leftGoal.trajectory.points[i].positions.resize(armDOF);
    for (size_t j = 0; j < armDOF; ++j){
      if(i==0){
	leftGoal.trajectory.points[i].positions[j] = state(j);
      }
      else if(i==1){
	leftGoal.trajectory.points[i].positions[j] = prediction(j);
      }
      else{
	leftGoal.trajectory.points[i].positions[j] = prediction(j) + (i-1) * period * control(j);
      
      }
    }
    
    // Velocities
    leftGoal.trajectory.points[i].velocities.resize(armDOF);
    for (size_t j = 0; j < armDOF; ++j){
      if(i==0){
	leftGoal.trajectory.points[i].velocities[j] = velocity(j);
      }
      else{
	leftGoal.trajectory.points[i].velocities[j] = control(j);
      }
    }

  }
  

  /// COMPUTE THE RIGHT ARM TRAJECTORY

  control_msgs::JointTrajectoryGoal rightGoal;

  for(int i = 0; i < armDOF; i++){
    rightGoal.trajectory.joint_names.push_back(joint_names[i+armDOF]);
  }

  
  rightGoal.trajectory.points.resize(K);

  for(int i = 0; i < K; i++){

    // To be reached 1 second after starting along the trajectory
    rightGoal.trajectory.points[i].time_from_start = ros::Duration(i * deadline);

    // trajectory point
    // Positions
    rightGoal.trajectory.points[i].positions.resize(armDOF);
    for (size_t j = 0; j < armDOF; ++j){
      if(i==0){
	rightGoal.trajectory.points[i].positions[j] = state(j+armDOF);
      }
      else if (i==1){
	rightGoal.trajectory.points[i].positions[j] = prediction(j+armDOF);
      }
      else{
	rightGoal.trajectory.points[i].positions[j] = prediction(j+armDOF)+ (i-1) * period * control(j+armDOF);

      }
    }
  
    // Velocities
    rightGoal.trajectory.points[i].velocities.resize(armDOF);
    for (size_t j = 0; j < armDOF; ++j){
      if(i==0){
	rightGoal.trajectory.points[i].velocities[j] = velocity(j+armDOF);
      }
      else{
	rightGoal.trajectory.points[i].velocities[j] = control(j+armDOF);
      }
    }
  }

  /// SEND THE TRAJECTORY TO THE ROBOT

  leftGoal.trajectory.header.stamp = ros::Time::now();
  rightGoal.trajectory.header.stamp = ros::Time::now();
  left_arm_client->sendGoal(leftGoal);
  right_arm_client->sendGoal(rightGoal);
 

  /// WAIT FOR THE GOAL ?
  if(waitFor){
    while(!left_arm_client->getState().isDone() && 
	  !right_arm_client->getState().isDone() &&
	  ros::ok())
      {
	usleep(50);
      }
  }

  
}



void PR2Arms::describeState() const
{
  for(int i = 0; i < totalDOF; i++){
    cout <<joint_names[i]<<"\t\t::=  "<<state(i)<<endl;
  }
}

