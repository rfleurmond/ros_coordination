#include "my_ros_camera.h"

using namespace Eigen;
using namespace std;


MyRosCamera::MyRosCamera(ros::NodeHandle node, string nom, string topic, const IntrinsicCam & param):
  Camera(param),
  name(nom),
  topicName(topic),
  myDisplay(nom),
  frameId("camera_optical_frame"),
  itr(node)
{
  image_sub = itr.subscribe(topicName,5,&MyRosCamera::imageCallback,this);
  myDisplay.setOptique(param);
  myDisplay.ouvrirFenetre();
}

MyRosCamera::~MyRosCamera(){
  myDisplay.fermerFenetre();
}

cv::Mat MyRosCamera::getLastImage() const{
  return lastImage;
}


CVEcran * MyRosCamera::getDisplay(){
  return &myDisplay;
}

void MyRosCamera::imageCallback(const sensor_msgs::ImageConstPtr & msg){

  cv_bridge::CvImagePtr cv_ptr;
  
  try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
  catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

  frameId = cv_ptr->header.frame_id;
    
  lastImage = cv::Mat(cv_ptr->image);
  myDisplay.setImage(lastImage);
  
  list<RealTracker *>::const_iterator it;
  RealTracker * pointeur = NULL;
  for(it = followers.begin(); it != followers.end(); it++ ){
    pointeur = * it;
    pointeur->attachToCamera(this);
    pointeur->track(lastImage);
    pointeur->draw(&myDisplay, cv::Scalar(0,0,255));
  }
  
  myDisplay.affichage();
  cv::waitKey(1);

}

void MyRosCamera::addFollowers(RealTracker * tracker){
  followers.push_back(tracker);
  tracker->initTracking(&myDisplay);
  while(ros::ok()){
    if(tracker->isRunning()){
      break;
    }
    ros::spinOnce();
  }
}

  
