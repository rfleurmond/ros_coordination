#include "tracker_manager.h"

using namespace Eigen;
using namespace std;


AssocTracker::AssocTracker():
  eye(NULL),
  cameraName(""),
  targetName(""),
  length(1),
  S(VectorXd::Zero(1)),
  seen(VectorXi::Zero(1)),
  lost(false),
  updates(0)
{
  
}

AssocTracker::AssocTracker(Camera * cam, std::string image, std::string target):
  eye(cam),
  cameraName(image),
  targetName(target),
  length(1),
  S(VectorXd::Zero(1)),
  seen(VectorXi::Zero(1)),
  lost(false)
{
  
}

SmallTracker::SmallTracker(TrackerManager * maitre, string name,int dimension):
  VTracker(),
  targetName(name),
  master(maitre),
  dim(dimension),
  libTracker(NULL)
{
  assert(maitre!=NULL);
  assert(dimension>0);
}


SmallTracker::SmallTracker(TrackerManager * maitre, string name, VTracker * tracker):
  VTracker(),
  targetName(name),
  master(maitre),
  libTracker(tracker)
{
  assert(tracker!=NULL);
  assert(tracker!=this);
  assert(maitre!=NULL);
 
  dim = libTracker->getDimensionOutput();
}

int SmallTracker::getDimensionOutput() const{
  return dim;
}

bool SmallTracker::itCanBeSeen() const{
  return true;
}

bool SmallTracker::isLost() const{
  string camera = master->cameras[oeil];
  return master->fonctions[camera+"_"+targetName].updates>0;
}

VectorXd SmallTracker::getFunction() const{
  string camera = master->cameras[oeil];
  return master->fonctions[camera+"_"+targetName].S;
}  

void SmallTracker::findFeatures(const VectorXd & SS){

  visual_coordination::MFeatures msg;

  string camera = master->cameras[oeil];
  
  msg.camera_name = camera;

  msg.target_name = targetName;

  msg.lost = 0;
  
  msg.length = SS.rows();

  for(int i = 0; i < msg.length; i++){
    msg.data.push_back(SS(i));
    msg.seen.push_back(1);
  }  

  msg.header.frame_id = master->frames[camera];

  msg.header.stamp = ros::Time::now();

  master->fonctions[camera+"_"+targetName].pub_track.publish(msg);

}

void SmallTracker::draw(CVEcran * const vue, cv::Scalar couleur) const{
  //DO NOTHING ?
  if(libTracker!=NULL){
    libTracker->drawFeat(getFunction(), vue, couleur);
  }
}

void SmallTracker::drawFeat(const VectorXd & value, CVEcran * vue, cv::Scalar couleur) const{
   //DO NOTHING ?
  if(libTracker!=NULL){
    libTracker->drawFeat(value, vue, couleur);
  }
}

TrackerManager::TrackerManager(){
}

TrackerManager::~TrackerManager(){
  while(indicateurs.size()>0){
    delete indicateurs.back();
    indicateurs.pop_back();
  }
}

void TrackerManager::trackerCallback(const visual_coordination::MFeaturesConstPtr& msg){
  frames[msg->camera_name] = msg->header.frame_id;
  string topic = msg->camera_name+"_"+msg->target_name;
  AssocTracker bound = fonctions[topic];
  bound.length = msg->length;
  bound.S = VectorXd::Zero(bound.length);
  for(int i = 0; i< bound.length; i++){
    bound.S(i) = msg->data[i];
  }
  bound.updates++;
  /*
  std::cout << "Where is the problem ? \n";
  std::cout << topic+" \n";
  std::cout << "Mises a jour  = "<<bound.updates<<std::endl;
  std::cout << "Longueur = "<<bound.length<<std::endl;
  std::cout << "Vecteur  = "<<bound.S.transpose()<<std::endl;
  */
  fonctions[topic] = bound;
}

void TrackerManager::addCamera(Camera * cam, std::string name){
  assert(cam!=NULL);
  cameras[cam] = name;
}

VTracker * TrackerManager::createTracker(string name, int dim){
  VTracker * track = new SmallTracker(this,name,dim);
  if(track!=NULL){
    indicateurs.push_back(track);
  }
  return track;  
}

VTracker * TrackerManager::createTracker(string name, VTracker * t){
  VTracker * track = new SmallTracker(this,name,t);
  if(track!=NULL){
    indicateurs.push_back(track);
  }
  return track;  
}

void TrackerManager::recordCouple(Camera * cam, string camName, string target, int dim){
  AssocTracker bound(cam, camName, target);
  string topicName =  camName+"_"+target;
  bound.S = VectorXd::Zero(dim);
  bound.sub_track = node.subscribe(topicName,100,&TrackerManager::trackerCallback,this);
  bound.pub_track = node.advertise<visual_coordination::MFeatures>(camName+"_"+target+"_reset", 1000); 
  fonctions[topicName] = bound;
}
  
