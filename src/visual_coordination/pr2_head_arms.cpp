#include "pr2_head_arms.h"

using namespace Eigen;
using namespace std;
 
PR2ArmsHead::PR2ArmsHead():
  PR2Arms(),
  headDOF(2)
{
  totalDOF = 16;
  state      = VectorXd::Zero(totalDOF);
  velocity   = VectorXd::Zero(totalDOF);
  control    = VectorXd::Zero(totalDOF);
  prediction = VectorXd::Zero(totalDOF);

  std::cout<<"Number of already registered joints :"<<joint_names.size()<<std::endl;
  
  joint_names.push_back("head_pan_joint");
  joint_names.push_back("head_tilt_joint");

  std::cout<<"Number of total registered joints :"<<joint_names.size()<<std::endl;
  
  head_client = new TrajClient("head_traj_controller/joint_trajectory_action", true);
  
  // wait for action server to come up
  while(!head_client->waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the head joint_trajectory_action server");
  }

  std::cout<<"Connection made in class PR2ArmsHead !"<<std::endl;
  
}

PR2ArmsHead::~PR2ArmsHead(){
  delete left_arm_client;
  delete right_arm_client;
  delete head_client;
}



void PR2ArmsHead::pushTrajectory(){

  
  /// COMPUTE THE LEFT ARM TRAJECTORY

  //std::cout << "Nouvel etat a atteindre=\n" << prediction.transpose() <<std::endl;

  control_msgs::JointTrajectoryGoal leftGoal;

  for(int i = 0; i < armDOF; i++){
    leftGoal.trajectory.joint_names.push_back(joint_names[i]);
  }

  leftGoal.trajectory.points.resize(K);

  
  for(int i = 0; i < K; i++){
  
    // To be reached 1 second after starting along the trajectory
    leftGoal.trajectory.points[i].time_from_start = ros::Duration(i * deadline);
    
    // trajectory point
    // Positions
    leftGoal.trajectory.points[i].positions.resize(armDOF);
    for (size_t j = 0; j < armDOF; j++){
      if(i==0){
	leftGoal.trajectory.points[i].positions[j] = state(j);
      }
      else if(i==1){
	leftGoal.trajectory.points[i].positions[j] = prediction(j);
      }
      else{
	leftGoal.trajectory.points[i].positions[j] = prediction(j) + (i-1) * period * control(j);
      }
    }

    
    
    // Velocities
    leftGoal.trajectory.points[i].velocities.resize(armDOF);
    for (size_t j = 0; j < armDOF; j++){
      if(i==0){
	leftGoal.trajectory.points[i].velocities[j] = velocity(j);
      }
      else{
	leftGoal.trajectory.points[i].velocities[j] = control(j);
      }

      //      leftGoal.trajectory.points[i].velocities[j] = 0;
    }
  }
  

  /// COMPUTE THE RIGHT ARM TRAJECTORY

  control_msgs::JointTrajectoryGoal rightGoal;

  for(int i = 0; i < armDOF; i++){
    rightGoal.trajectory.joint_names.push_back(joint_names[i+armDOF]);
  }

  
  rightGoal.trajectory.points.resize(K);

  for(int i = 0; i < K; i++){

    // To be reached 1 second after starting along the trajectory
    rightGoal.trajectory.points[i].time_from_start = ros::Duration(i * deadline);

    // trajectory point
    // Positions
    rightGoal.trajectory.points[i].positions.resize(armDOF);
    for (size_t j = 0; j < armDOF; j++){
      if(i==0){
	rightGoal.trajectory.points[i].positions[j] = state(j+armDOF);
      }
      else if (i==1){
	rightGoal.trajectory.points[i].positions[j] = prediction(j+armDOF);
      }
      else{
	rightGoal.trajectory.points[i].positions[j] = prediction(j+armDOF)+ (i-1) * period * control(j+armDOF);

      }
    }
  

    // Velocities
    rightGoal.trajectory.points[i].velocities.resize(armDOF);
    for (size_t j = 0; j < armDOF; j++){
      if(i==0){
	rightGoal.trajectory.points[i].velocities[j] = velocity(j+armDOF);
      }
      else{
	rightGoal.trajectory.points[i].velocities[j] = control(j+armDOF);
      }

      //rightGoal.trajectory.points[i].velocities[j] = 0;
 
    }
  }


  control_msgs::JointTrajectoryGoal headGoal;

  for(int i = 0; i < headDOF; i++){
    headGoal.trajectory.joint_names.push_back(joint_names[i+2*armDOF]);
  }

  int debut = 2 * armDOF;

  headGoal.trajectory.points.resize(K);

  for(int i = 0; i < K; i++){

    // To be reached 1 second after starting along the trajectory
    headGoal.trajectory.points[i].time_from_start = ros::Duration(i * deadline);

    // trajectory point
    // Positions
    headGoal.trajectory.points[i].positions.resize(headDOF);
    for (size_t j = 0; j < headDOF; j++){
      if(i==0){
	headGoal.trajectory.points[i].positions[j] = state(j + debut);
      }
      else if (i==1){
	headGoal.trajectory.points[i].positions[j] = prediction(j + debut);
      }
      else{
	headGoal.trajectory.points[i].positions[j] = 
	  prediction(j + debut) + 
	  (i-1) * period * control(j + debut);

      }
    }
  
    // Velocities
    headGoal.trajectory.points[i].velocities.resize(headDOF);
    for (size_t j = 0; j < headDOF; j++){
      //headGoal.trajectory.points[i].velocities[j] = 0;

      //*
      if(i==0){
	headGoal.trajectory.points[i].velocities[j] = velocity(j + debut);
      }
      else{
	headGoal.trajectory.points[i].velocities[j] = control( j + debut);
      }
      //*/
    }
  }


  /// SEND THE TRAJECTORY TO THE ROBOT

  leftGoal.trajectory.header.stamp = ros::Time::now();
  rightGoal.trajectory.header.stamp = ros::Time::now();
  headGoal.trajectory.header.stamp = ros::Time::now();
  
  left_arm_client->sendGoal(leftGoal);
  right_arm_client->sendGoal(rightGoal);

  if(waitFor)
    head_client->sendGoal(headGoal);
  
  /// WAIT FOR THE GOAL ?
  if(waitFor){
    while(!left_arm_client->getState().isDone() && 
	  !right_arm_client->getState().isDone() &&
	  !head_client->getState().isDone() &&
	  ros::ok())
      {
	usleep(50);
      }
  }

}

