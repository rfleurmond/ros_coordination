#include "complex_tracker.h"

using namespace Eigen;
using namespace std;

ComplexTracker::ComplexTracker(std::string name, VTracker * tracker):
  VTracker(),
  targetName(name),
  master(tracker)
{
  assert(tracker!=NULL);
}

void ComplexTracker::addInternalTracker(Camera * cam, VTracker * track){
  assert(cam!=NULL);
  assert(track!=NULL);
  employees[cam] = track;
}

int ComplexTracker::getDimensionOutput() const{
  return master->getDimensionOutput();
}

bool ComplexTracker::itCanBeSeen() const{
  if(employees.count(oeil)>0)
    return employees.at(oeil)->itCanBeSeen();
  else
    return false;
}

bool ComplexTracker::isLost() const{
  if(employees.count(oeil)>0)
    return employees.at(oeil)->isLost();
  else
    return false;
}

VectorXd ComplexTracker::getFunction() const{
  VectorXd S;
  if(employees.count(oeil)>0)
    S = employees.at(oeil)->getFunction();
  else
    S =  master->getFunction();
  return S;
}

void ComplexTracker::findFeatures(const VectorXd & SS){
  if(employees.count(oeil)>0)
    employees.at(oeil)->findFeatures(SS);
}  

void ComplexTracker::draw(CVEcran * const vue, cv::Scalar couleur) const{
  if(employees.count(oeil)>0)
    employees.at(oeil)->draw(vue,couleur);
}

void ComplexTracker::drawFeat(const VectorXd & value, CVEcran * const vue, cv::Scalar couleur) const{
  master->drawFeat(value,vue,couleur);
}
