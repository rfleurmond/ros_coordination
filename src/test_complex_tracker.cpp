#include "complex_tracker.h" 
#include "my_ros_camera.h" 


int main(int argc, char** argv)
{
  ros::init(argc, argv, "complex_cyl_tracker");

  ros::NodeHandle node;
  
  if(argc>3){
    std::string topic(argv[1]);
    std::string camera(argv[2]);
    std::string target(argv[3]);

    IntrinsicCam headRight(417.66329, 417.88768, 318.43842, 221.30017);
  
    MyRosCamera eye(node,camera,topic,headRight);

    CylinderTracker voyeur;

    eye.addFollowers(&voyeur);

    ComplexTracker cible(target,&voyeur);

    cible.addInternalTracker(&eye,&voyeur);

    std::cout << "Tracker initialise !! Démarrage de ros::spin() " << std::endl;
  
    while(ros::ok()){
	
      ros::spinOnce();
      cible.getFunction();
    }
  }
  else{
    std::cerr << "You should provide three arguments to this node \n"
	      << "One example of a correct command is:\n"
	      << "rosrun visual_coordination cyl_tracker /camera_topic camera_name target_name"
	      << std::endl;
  }

  return 0;
}
