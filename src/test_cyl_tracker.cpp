#include "cylinder_tracker.h" 


int main(int argc, char** argv)
{
  ros::init(argc, argv, "cyl_tracker");
  
  if(argc>3){
    std::string topic(argv[1]);
    std::string camera(argv[2]);
    std::string target(argv[3]);
    CylTracker st(topic,"Display_"+camera+"_"+target,camera,target);
    ros::spin();
  }
  else{
    std::cerr << "You should provide three arguments to this node \n"
	      << "One example of a correct command is:\n"
	      << "rosrun visual_coordination cyl_tracker /camera_topic camera_name target_name"
	      << std::endl;
  }

  return 0;
}
