#include "ros_robot_system.h" 

int main(int argc, char** argv)
{
  // Init the ROS node
  ros::init(argc, argv, "renliw_pr2");
  
  Eigen::VectorXd but(14);

  but << 
    M_PI/4,  0.1,  M_PI/2,  -M_PI/2, -M_PI/2,  -0.05,  0,
    -M_PI/4, -0.1, -M_PI/2,  -M_PI/2,  M_PI/2,  -0.05,  0;
  
  PR2Arms C3P0;

  C3P0.setState(but);

  C3P0.describeState();

  ros::spin();
}
