#include "ros_task_controller.h"
#include "ros_robot_system.h"
#include "pr2_head_arms.h"
#include "tracker_manager.h"
#include "my_ros_camera.h"
#include "complex_tracker.h"
#include "vservoing.h"

using namespace Eigen;
using namespace std;

int main(int argc, char** argv)
{
  // Init the ROS node
  ros::init(argc, argv, "renliw_coord");

  ros::NodeHandle node;
  
  VectorXd but(16);

  but << 
    M_PI/4,   0.1,  M_PI/2,  -M_PI/2, -M_PI/2,  -0.05,  0,
    -M_PI/4, -0.1, -M_PI/2,  -M_PI/2,  M_PI/2,  -0.05,  0,
    0,        0.6;
  
  double lengthCap = 0.075;
  double lengthPen = 0.1;
  double radius = 0.01;
  
  double r2d = 360/M_PI;
   
  IntrinsicCam headRight(417.66329, 417.88768, 318.43842, 221.30017);
  IntrinsicCam headLeft( 421.96246, 422.28643, 317.76621, 224.33886);
  IntrinsicCam headNarrowLeft(921.74417, 922.03242, 318.18217, 229.60525);
  IntrinsicCam headNarrowRight(926.92009, 927.28029, 296.08205, 237.21265);
  IntrinsicCam rightArm( 430.67783, 430.19755, 324.72594, 236.54259);
  IntrinsicCam leftArm(  424.97353, 427.11019, 324.51727, 228.79747);
 
  MyRosCamera rightEye(node,"RightEye","/narrow_stereo/right/image_raw",headNarrowRight);
  MyRosCamera leftEye(node,"LeftEye","/narrow_stereo/left/image_raw",headNarrowLeft);
  MyRosCamera camRarm(node,"RArmEye","/r_forearm_cam/image_rect_color",rightArm);
  MyRosCamera camLarm(node,"LArmEye","/l_forearm_cam/image_rect_color",rightArm);
  
  
  BoundedCylinder cap(lengthCap,radius);
  BoundedCylinder pen(lengthPen,radius);
  
  CylinderTracker trackRightCap;
  CylinderTracker trackLeftPen;
  CylinderTracker trackArmCap;
  CylinderTracker trackArmPen;
  

  
  ComplexTracker capTracker("cap",&trackRightCap);
  capTracker.addInternalTracker(&rightEye,&trackRightCap);
  capTracker.addInternalTracker(&camRarm,&trackArmCap);
  
  ComplexTracker penTracker("pen",&trackLeftPen);
  penTracker.addInternalTracker(&leftEye,&trackLeftPen);
  penTracker.addInternalTracker(&camLarm,&trackArmPen);

 
  //*
  BCylinderEstimator capEstimator(&cap,&capTracker);

  BCylinderEstimator penEstimator(&pen,&penTracker);

  // */

  /*

  BCylinderEstimator capEstimator(&cap,&cap);

  BCylinderEstimator penEstimator(&pen,&pen);

  // */

  //capEstimator.setRadius(radius);

  //penEstimator.setRadius(radius);

  capEstimator.setLength(lengthCap);

  penEstimator.setLength(lengthPen);

  capEstimator.selectInformation(0,0,1,1);

  penEstimator.selectInformation(0,0,1,1);

  capEstimator.setLogFile("estimation-cap.dat");

  capEstimator.setDeadline(15.0);

  penEstimator.setLogFile("estimation-pen.dat");

  penEstimator.setDeadline(15.0);

  //Gestion des bras du robot

  //Creation des modeles vides et des bras

  GeoRobot brasGauche,brasDroite,couPR2;

  //Chargement du modele geometrique des bras

  loadSE3Robot("/home/rfleurmo/Documents/THESE_LAAS/CODES/C++/robot_models/pr2_gauche.ser",brasGauche);

  loadSE3Robot("/home/rfleurmo/Documents/THESE_LAAS/CODES/C++/robot_models/pr2_droite.ser",brasDroite);

  loadSE3Robot("/home/rfleurmo/Documents/THESE_LAAS/CODES/C++/robot_models/pr2_head.ser",couPR2);


  //Creation des modeles cinematiques des bras

  MyKineModel modeleGauche(&brasGauche);

  MyKineModel modeleDroite(&brasDroite);

  MyKineModel modeleTete(&couPR2);


  ManyArmHolderTask tache(true);

  // Defintion des reperes

  Repere epauleGauche(VectRotation(0,0,0),Vector3d(0,  0.188, 0));
  
  Repere epauleDroite(VectRotation(0,0,0),Vector3d(0, -0.188, 0));

  Repere priseGauche(VectRotation(0, 0, 0),Vector3d(0.17, 0, 0));
  
  Repere priseDroite(VectRotation(0, 0, 0),Vector3d(0.17, 0, 0.00));

  Repere narrowLStereo(VectRotation(0,0,0),Vector3d(0.068, 0.06, 0.115));

  Repere narrowRStereo(VectRotation(0,0,0),Vector3d(0.068, -0.03, 0.115));
  
  Repere wideLStereo(VectRotation(0,0,0),Vector3d(0.068, 0.03, 0.115));

  Repere wideRStereo(VectRotation(0,0,0),Vector3d(0.068, -0.06, 0.115));

  Repere forearmRCam(VectRotation(3.018, -0.872524,0),
		     Vector3d(0.135-0.321, 0.044, 0));

  Repere forearmLCam(VectRotation(0,0,-0.562866),
		     Vector3d(0.135-0.321, 0.044, 0));

  Repere identite;

  // Ajout des deux bras a la tache

  tache.addArm(&modeleGauche,epauleGauche);

  tache.addArm(&modeleDroite,epauleDroite);

  tache.addArm(&modeleTete, identite);

  // Defintion des parametres systemes de la tache

  tache.setPeriod(1.0/15);

  double lambda = 0.2;

  double lambdaF = 0.5;

  tache.setLambda(lambda);

  tache.setMinimumErrorNorm(1e-2);

  //Placement des caméras

  tache.addCamera(&rightEye,&modeleTete,narrowRStereo,2,"Narrow Right Camera");

  tache.addCamera(&leftEye,&modeleTete,narrowLStereo,2,"Narrow Left camera");

  tache.addCamera(&camRarm,&modeleDroite,forearmRCam,5,"Right Forearm Camera");

  tache.addCamera(&camLarm,&modeleGauche,forearmLCam,5,"Left Forearm Camera");

  // Placement des indices visuels

  tache.addVisualTarget(&capEstimator,&modeleGauche,priseGauche,7);

  tache.addVisualTarget(&penEstimator,&modeleDroite,priseDroite,7);

  // Quels indices visuels percoivent les cameras?

  tache.makeTargetSeen(&capEstimator,&leftEye);

  tache.makeTargetSeen(&capEstimator,&rightEye);

  tache.makeTargetSeen(&penEstimator,&leftEye);

  tache.makeTargetSeen(&penEstimator,&rightEye);

  tache.makeTargetSeen(&capEstimator,&camRarm);

  tache.makeTargetSeen(&penEstimator,&camRarm);

  tache.makeTargetSeen(&capEstimator,&camLarm);

  tache.makeTargetSeen(&penEstimator,&camLarm);

  // Quelle est la reference des indices visuels par camera?
  
  
  //tache.giveKandCmatrix(&rightEye,&capEstimator,MatrixXd::Identity(8,8),VectorXd::Ones(8));

  
  /// CONNECTION AU ROBOT PR2
  
  PR2ArmsHead C3P0;

  C3P0.enableControl(false);

  C3P0.setState(but);

  C3P0.describeState();

  tache.setState(C3P0.getState());
  
  //*

  rightEye.addFollowers(&trackRightCap);
  camRarm.addFollowers(&trackArmCap);
  leftEye.addFollowers(&trackLeftPen);
  camLarm.addFollowers(&trackArmPen);

  //*/

  MyClock::start(ros::Time::now().toSec());
  
  while(ros::ok()){
    
    ros::spinOnce();
    
    MyClock::setTime(ros::Time::now().toSec());

    tache.drawVisualFeatures();

    if(MyClock::getTime()>10)
      break;
    
  }

  return 0;

}
