#include "gripper.h"

int main(int argc, char** argv){

  ros::init(argc, argv, "openLeft");

  Gripper gripper(Gripper::LEFT_GRIPPER);

  gripper.open();

  return 0;
}
