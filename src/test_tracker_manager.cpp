#include "tracker_manager.h" 

int main(int argc, char** argv)
{
  ros::init(argc, argv, "tracker_listener");
  
  TrackerManager client_serveur;

  Camera oeil;

  VTracker *  tracker = client_serveur.createTracker("Thing",4);

  client_serveur.recordCouple(&oeil,"Eye","Thing");

  ros::spin();
  
  return 0;
}
