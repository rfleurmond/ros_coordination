#include "single_tracker.h" 


int main(int argc, char** argv)
{
  ros::init(argc, argv, "empty_tracker");
  
  if(argc>1){
    std::string topic(argv[1]);
    SingleTracker st(topic,"OpenCV","Renliw","Empty");
    ros::spin();
  }
  else{
    std::cerr << "You should provide one argument to this node \n"
	      << "One example of a correct command is:\n"
	      << "rosrun visual_coordination empty_tracker /camera_topic"
	      << std::endl;
  }

  return 0;
}
