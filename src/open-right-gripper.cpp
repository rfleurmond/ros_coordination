#include "gripper.h"

int main(int argc, char** argv){

  ros::init(argc, argv, "openRight");

  Gripper gripper(Gripper::RIGHT_GRIPPER);

  gripper.open();

  return 0;
}
