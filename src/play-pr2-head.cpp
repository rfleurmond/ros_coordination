#include <ros/ros.h>
#include <control_msgs/JointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include "outils.h"
#include <iostream>

typedef actionlib::SimpleActionClient< control_msgs::JointTrajectoryAction > TrajClient;

class PR2HeadArms
{
private:
  // Action client for the joint trajectory action 
  // used to trigger the arm movement action
  TrajClient* left_arm_client_;
  TrajClient* right_arm_client_;
  TrajClient* head_client_;
  
  int temps;
  int cmdGauche;
  int cmdDroite;
  int cmdTete;
  int qGauche;
  int qDroite;
  int qTete;
  int periode;
  double pause;
  std::vector<double *> data;
  

public:
  //! Initialize the action client and wait for action server to come up
  PR2HeadArms() 
  {

    temps = 0;
    cmdGauche =   1;//1 pour le temps 
    cmdDroite =   8;// 1+7 degres d'un bras = 7
    cmdTete   =  15;// 8 +7
    qGauche   =  17; //15 + 2 (degres de la tete = 2)
    qDroite   =  24;//17+7
    qTete     =  31;//24+7
    periode = 1;
    pause = 2.0 ; // 2 secondes

    data = loadDAT("/home/rfleurmo/Documents/THESE_LAAS/CODES/C++/build/data_sys.dat",33);//31+2

    std::cout<<"Nombre d'iterations "<<data.size()<<std::endl;
    
    // tell the action client that we want to spin a thread by default
    left_arm_client_ = new TrajClient("l_arm_controller/joint_trajectory_action", true);
    right_arm_client_ = new TrajClient("r_arm_controller/joint_trajectory_action", true);
    head_client_ = new TrajClient("head_traj_controller/joint_trajectory_action", true);

    // wait for action server to come up
    while(!left_arm_client_->waitForServer(ros::Duration(5.0))){
      ROS_INFO("Waiting for the joint_trajectory_action server");
    }
    while(!right_arm_client_->waitForServer(ros::Duration(5.0))){
      ROS_INFO("Waiting for the joint_trajectory_action server");
    }
    while(!head_client_->waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the head joint_trajectory_action server");
    }
    

    std::cout<<"Connection aux deux serveurs!? "<<std::endl;
    
  }

  //! Clean up the action client
  ~PR2HeadArms()
  {
    delete left_arm_client_;
    delete right_arm_client_;
    delete head_client_;
    freeDATA(data);
  }

  //! Sends the command to start a given trajectory
  void startTrajectory(
		       control_msgs::JointTrajectoryGoal leftGoal,
		       control_msgs::JointTrajectoryGoal rightGoal,
		       control_msgs::JointTrajectoryGoal headGoal)
  {
    // When to start the trajectory: 1s from now
    leftGoal.trajectory.header.stamp  = ros::Time::now() + ros::Duration(1.0);
    rightGoal.trajectory.header.stamp = leftGoal.trajectory.header.stamp;
    headGoal.trajectory.header.stamp  = leftGoal.trajectory.header.stamp;
    left_arm_client_->sendGoal(leftGoal);
    right_arm_client_->sendGoal(rightGoal);
    head_client_->sendGoal(headGoal);
  }

  //! Generates a simple trajectory with two waypoints, used as an example
  /*! Note that this trajectory contains two waypoints, joined together
      as a single trajectory. Alternatively, each of these waypoints could
      be in its own trajectory - a trajectory can have one or more waypoints
      depending on the desired application.
  */

  void fillTrajectory(control_msgs::JointTrajectoryGoal & goal, int sens)
  {
    //sens = 0 pour droite 1, pour gauche 2 pour la tete
    
    // We will have two waypoints in this goal trajectory
    goal.trajectory.points.resize(data.size());
    
    // First trajectory point
    goal.trajectory.points[0].time_from_start = ros::Duration(pause);

    // Positions
    int ind = 0;
    double * instant = NULL;

    int position = qDroite;
    int vitesses = cmdDroite;

    int dim = 7;
    
    if(sens==1){
      position = qGauche;
      vitesses = cmdGauche;
    }
    else if(sens==2){
      position = qTete;
      vitesses = cmdTete;
      dim = 2;
    }

    std::vector<double * >::iterator iterateur;

    for(iterateur = data.begin();iterateur!=data.end();iterateur++){
      
      double * instant = *iterateur;

      // Positions
      goal.trajectory.points[ind].positions.resize(dim);
      for (size_t j = 0; j <dim; ++j){
	goal.trajectory.points[ind].positions[j] = instant[position+j];
      }
    
      goal.trajectory.points[ind].time_from_start = ros::Duration(pause + instant[temps]);

      /*
      // Velocities
      goal.trajectory.points[ind].velocities.resize(7);
      for (size_t j = 0; j < 7; ++j){
	goal.trajectory.points[ind].velocities[j] = instant[vitesses+j];
      }
      */
      ind+=periode;
      
    }

    goal.trajectory.points[data.size()-1].velocities.resize(dim);
    goal.trajectory.points[0].velocities.resize(dim);
    for (size_t j = 0; j < dim; ++j){
      goal.trajectory.points[data.size()-1].velocities[j] = 0.0;
      goal.trajectory.points[0].velocities[j] = 0.0;
    }
    
  }



  control_msgs::JointTrajectoryGoal designRightArmTrajectory()
  {
    //our goal variable
    control_msgs::JointTrajectoryGoal goal;

    // First, the joint names, which apply to all waypoints
    goal.trajectory.joint_names.push_back("r_shoulder_pan_joint");
    goal.trajectory.joint_names.push_back("r_shoulder_lift_joint");
    goal.trajectory.joint_names.push_back("r_upper_arm_roll_joint");
    goal.trajectory.joint_names.push_back("r_elbow_flex_joint");
    goal.trajectory.joint_names.push_back("r_forearm_roll_joint");
    goal.trajectory.joint_names.push_back("r_wrist_flex_joint");
    goal.trajectory.joint_names.push_back("r_wrist_roll_joint");
    
    fillTrajectory(goal, 0);
      
    //we are done; return the goal
    return goal;
  }

  control_msgs::JointTrajectoryGoal designLeftArmTrajectory()
  {
    //our goal variable
    control_msgs::JointTrajectoryGoal goal;

    // First, the joint names, which apply to all waypoints
    goal.trajectory.joint_names.push_back("l_shoulder_pan_joint");
    goal.trajectory.joint_names.push_back("l_shoulder_lift_joint");
    goal.trajectory.joint_names.push_back("l_upper_arm_roll_joint");
    goal.trajectory.joint_names.push_back("l_elbow_flex_joint");
    goal.trajectory.joint_names.push_back("l_forearm_roll_joint");
    goal.trajectory.joint_names.push_back("l_wrist_flex_joint");
    goal.trajectory.joint_names.push_back("l_wrist_roll_joint");
    
    fillTrajectory(goal, 1);
      
    //we are done; return the goal
    return goal;
  }


control_msgs::JointTrajectoryGoal designHeadTrajectory()
  {
    //our goal variable
    control_msgs::JointTrajectoryGoal goal;

    // First, the joint names, which apply to all waypoints
    goal.trajectory.joint_names.push_back("head_pan_joint");
    goal.trajectory.joint_names.push_back("head_tilt_joint");
    
    fillTrajectory(goal, 2);
      
    //we are done; return the goal
    return goal;
  }

  //! Returns the current state of the action
  actionlib::SimpleClientGoalState getLeftState()
  {
    return left_arm_client_->getState();
  }

  actionlib::SimpleClientGoalState getRightState()
  {
    return right_arm_client_->getState();
  }

  actionlib::SimpleClientGoalState getHeadState()
  {
    return head_client_->getState();
  }
 
};

int main(int argc, char** argv)
{
  // Init the ROS node
  std::cout<<"Initialisation du noeur ROS"<<std::endl;
  ros::init(argc, argv, "robot_driver");
  std::cout<<"Lancement du contructeur"<<std::endl;
  PR2HeadArms PR2;
  // Start the trajectory
  

  std::cout<<"Créer la trajectoire du bras gauche"<<std::endl;

  control_msgs::JointTrajectoryGoal leftGoal = PR2.designLeftArmTrajectory();

  std::cout<<"Créer la trajectoire du bras droit"<<std::endl;

  control_msgs::JointTrajectoryGoal rightGoal = PR2.designRightArmTrajectory();

  std::cout<<"Créer la trajectoire de la tete"<<std::endl;

  control_msgs::JointTrajectoryGoal headGoal = PR2.designHeadTrajectory();

  std::cout<<"Demarrer la trajectoire"<<std::endl;
  
  PR2.startTrajectory( leftGoal, rightGoal, headGoal );
  
  // Wait for trajectory completion
  std::cout<<"Juste avant la boucle"<<std::endl;
  while(
	!PR2.getLeftState().isDone() && 
	!PR2.getRightState().isDone() && 
	!PR2.getHeadState().isDone() && 
	ros::ok())
  {
    usleep(50000);
  }
}
